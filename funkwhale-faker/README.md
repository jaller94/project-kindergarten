# Funkwhale Faker

This ExpressJS server imitates the Federation API of the music server Funkwhale.

Funkwhale Federation: https://docs.funkwhale.audio/federation/index.html

Goals:
* Host music libraries without having to run Funkwhale
* Dynamically generate libraries from external music sources (e.g. an RSS feed of a podcast)
