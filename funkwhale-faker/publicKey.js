const crypto = require('crypto');
const { promisify } = require('util');

const generateKeyPairPromise = promisify(crypto.generateKeyPair);


async function genKey() {
    const pair = await generateKeyPairPromise('rsa', {
        modulusLength: 4096,
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
        },
    });
    return pair;
}

module.exports = {
    genKey,
};
