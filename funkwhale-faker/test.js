'use strict';
const sa = require('superagent');

const baseUrl = 'http://localhost:8080';

describe('actor', () => {
    test('Actor', async() => {
        const res = await sa(`${baseUrl}/actors/jaller94`);
        const body = JSON.parse(res.body);
        expect(body.name).toBe('jaller94');
        expect(body.type).toBe('Person');
        expect(typeof body.preferredUsername === 'string').toBe(true);
        expect(typeof body.name === 'string').toBe(true);
        expect(typeof body.manuallyApprovesFollowers === 'boolean').toBe(true);
    });
});

describe('page', () => {
    test('Library', async () => {
        const res = await sa(`${baseUrl}/music/libraries/1`);
        const body = JSON.parse(res.body);
        expect(body.type).toBe('Library');
    });
    test('CollectionPage', async () => {
        const res = await sa(`${baseUrl}/music/libraries/1?page=1`);
        const body = JSON.parse(res.body);
        expect(body.type).toBe('CollectionPage');
        expect(body.first).toBe(body.last);
        expect(body.first).toBe(body.id);
        expect(Array.isArray(body.items)).toBe(true);
        expect(body.items).toHaveLength(body.totalItems);
        expect(body.items[0].type).toBe('Audio');
        expect(body.items[0].track.type).toBe('Track');
        expect(body.items[0].track.artists[0].type).toBe('Artist');
        expect(body.items[0].track.album.type).toBe('Album');
        expect(body.items[0].track.album.artists[0].type).toBe('Artist');
    });
});