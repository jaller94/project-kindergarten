'use strict';
const express = require('express');
const app = express();
const port = 8080;

const baseUrl = 'http://37.120.46.137';

// Remove header for security
app.disable('x-powered-by');

// For parsing application/json
app.use(express.json());

// For parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
    console.log({
        method: req.method,
        url: req.url,
        query: req.query,
        body: req.body,
        headers: req.headers,
    });
    next();
});

app.get('/', (req, res) => {
    res.end('Hello World!');
});

app.use(express.static('public'));

app.get('/ipfs/QmPiMEZtgqKraCA9JLNJrb7gTzCyh4kwncFSPFyi3fJvM9', (req, res) => {
    res.sendFile('Life is Fun.mp3', { root: __dirname + '/public' });
});

const boyinaband = {
    type: 'Artist',
    id: `${baseUrl}/music/artists/1`,
    name: 'Boyinaband',
    published: '2020-09-29T00:42:46+00:00',
    musicbrainzId: 'bcd873d3-fd99-4c0a-99ae-a4f75f71c06a',
    attributedTo: `${baseUrl}/actors/jaller94`,
    tag: [],
    image: null,
};

const album = {
    type: 'Album',
    id: `${baseUrl}/music/albums/1`,
    name: 'Life is Fun (with TheOdd1sOut)',
    published: '2020-09-29T00:42:46.658914+00:00',
    musicbrainzId: '220a75d0-b1f6-4043-b955-bd2d4538ddbb',
    released: '2019-01-01',
    attributedTo: `${baseUrl}/actors/jaller94`,
    tag: [],
    artists: [
        boyinaband,
    ],
    // 'content': '<p>Placeholder text</p>',
    // 'mediaType': 'text/html',
    image: {
        type: 'Image',
        url: `${baseUrl}/mbid-220a75d0-b1f6-4043-b955-bd2d4538ddbb-25120868203.jpg`,
        mediaType: 'image/jpeg',
    }
};

const uploads = [
    {
        type: 'Audio',
        id: `${baseUrl}/music/uploads/1`,
        library: `${baseUrl}/music/libraries/1`,
        name: 'BoyinaBand - Life is Fun (with TheOdd1sOut)',
        published: '2020-09-29T00:43:48.859107+00:00',
        bitrate: 136000,
        size: 4113453,
        duration: 241,
        url: [
            {
                href: `${baseUrl}/ipfs/QmPiMEZtgqKraCA9JLNJrb7gTzCyh4kwncFSPFyi3fJvM9`,
                type: 'Link',
                mediaType: 'audio/mpeg',
            },
            {
                type: 'Link',
                mediaType: 'text/html',
                href: `${baseUrl}/library/tracks/201`,
            },
        ],
        track: {
            type: 'Track',
            id: `${baseUrl}/music/tracks/1`,
            name: 'Life is Fun',
            published: '2020-09-29T00:42:46+00:00',
            musicbrainzId: 'fb39bafe-f7a8-4478-a120-27350e69f790',
            position: 1,
            disc: null,
            license: null,
            copyright: null,
            artists: [
                boyinaband,
            ],
            album,
            attributedTo: `${baseUrl}/actors/jaller94`,
            tag: [],
            // 'content: '',
            // 'mediaType: 'text/html',
            image: null,
        },
        to: 'https://www.w3.org/ns/activitystreams#Public',
        attributedTo: `${baseUrl}/actors/jaller94`,
        updated: '2020-09-29T00:43:48.859119+00:00',
    },
];

app.get('/music/libraries/1', (req, res) => {
    let answer = {
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            'https://funkwhale.audio/ns',
            {
                manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
                Hashtag: 'as:Hashtag'
            },
        ],
        id: `${baseUrl}/music/libraries/1`,
        attributedTo: `${baseUrl}/actors/jaller94`,
        totalItems: uploads.length,
        type: req.query.page ? 'CollectionPage' : 'Library',
        first: `${baseUrl}/music/libraries/1?page=1`,
        last: `${baseUrl}/music/libraries/1?page=1`,
        name: 'Funkwhale Test',
        summary: 'A library for testing purposes.',
    };
    if (req.query.page) {
        answer = {
            ...answer,
            id: `${baseUrl}/music/libraries/1?page=${req.query.page}`,
            partOf: `${baseUrl}/music/libraries/1`,
            // next: null,
            // prev: null,
            items: uploads,
        };
    } else {
        answer = {
            ...answer,
            audience: 'https://www.w3.org/ns/activitystreams#Public',
            current: `${baseUrl}/music/libraries/1?page=1`,
            followers: `${baseUrl}/music/libraries/1/followers`,
        };
    }
    res.type('application/activity+json').json(answer);
});

app.get('/music/libraries/:id/followers', (req, res) => {
    res.type('application/activity+json').json({});
});

app.get('/albums/1', (req, res) => {
    res.type('application/activity+json').json({
        ...album,
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            'https://funkwhale.audio/ns',
            {
                manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
                Hashtag: 'as:Hashtag',
            },
        ],
    });
});

app.get('/actors/jaller94', (req, res) => {
    res.type('application/activity+json').json({
        id: `${baseUrl}/actors/jaller94`,
        outbox: `${baseUrl}/actors/jaller94/outbox`,
        inbox: `${baseUrl}/actors/jaller94/inbox`,
        preferredUsername: 'jaller94',
        type: 'Person',
        name: 'jaller94',
        followers: `${baseUrl}/actors/jaller94/followers`,
        following: `${baseUrl}/actors/jaller94/following`,
        manuallyApprovesFollowers: false,
        url: [
            {
                type: 'Link',
                href: `${baseUrl}/@jaller94`,
                mediaType: 'text/html',
            }
        ],
        icon: {
            type: 'Image',
            url: `${baseUrl}/media/attachments/b6/7c/c5/current.jpg`,
            mediaType: 'image/jpeg',
        },
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            'https://funkwhale.audio/ns',
            {
                manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
                Hashtag: 'as:Hashtag',
            }
        ],
        publicKey: {
            owner: `${baseUrl}/actors/jaller94`,
            publicKeyPem: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEApsxRYIOJ9vch+rvpbF4f\n4Fa4HVdSJCW3dUO50cSt4CDTGpf6177n/sN1xqRqqFyNpfFIBKXjXWM1mJOZUy48\nvoEJwMPVRfArL5S979FTKdZnrtIQpHYzXBchrBxQr9koDPHqsOesumu+gwIKIOmx\nYreYsljhRAVsQpefim/SpS0N4F6LRK+3/g0YYZtvD/wrtDOreIWCRtI1zHjhwbHV\npkIeUEp2RRf8J0z+tRDiVh7djtbbdRqtuDazxalq5dO9GW9j1q5yl0rZUqCOHJW9\nSYFGzi1KHTCuTvoiaXIjZta8YudAxeKX4eZTQ8LgsZurHiSbReH4iPa+8maZoXX2\nlGC170xV5VJmsROsmO3fnbbc/Io8r3gY4uh22CDkCUSwCsrCZQRLBsdw00IVtZtp\n3POxCxti3KFHNYknYP4AdcNR/fSE/KLb+MBZRntqowcpXDIcN4tHixIQIOqbl7aK\nI80qOBwn69/q0T4772UXXxDzuGJ27xL5tslJ20zAdk7oasNOlZeWudfxV8nwWMU/\nwp5PT/WFLephGHSWey5tYPzNNHV13LrXzlMS07zUNH64UwEN7cl3ATTqIX3oPyTV\nSmiIFsGFe0v0PviQTCQxkd+3y4wzgVUaEBHznQwZbCVLq/PFJki7tHqQDtwjciVR\ncAUCNaUW8185YfWwTNuDZBECAwEAAQ==\n-----END PUBLIC KEY-----\n',
            id: `${baseUrl}/actors/jaller94#main-key`,
        },
        endpoints: {
            sharedInbox: `${baseUrl}/shared/inbox`,
        }
    });
});

app.get('/actors/:id/followers', (req, res) => {
    res.type('application/activity+json').json({});
});

app.get('/actors/:id/following', (req, res) => {
    res.type('application/activity+json').json({});
});

app.get('/actors/:id/inbox', (req, res) => {
    res.type('application/activity+json').json({});
});

app.get('/actors/:id/outbox', (req, res) => {
    res.type('application/activity+json').json({});
});

app.get('/music/uploads/1', (req, res) => {
    res.type('application/activity+json').json({
        ...uploads[0],
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            'https://funkwhale.audio/ns',
            {
                manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
                Hashtag: 'as:Hashtag',
            },
        ],
    });
});

app.get('/music/tracks/1', (req, res) => {
    res.type('application/activity+json').json({
        ...uploads[0].track,
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            'https://funkwhale.audio/ns',
            {
                manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
                Hashtag: 'as:Hashtag',
            },
        ],
    });
});

app.get('/music/artists/1', (req, res) => {
    res.type('application/activity+json').json({
        ...boyinaband,
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            'https://funkwhale.audio/ns',
            {
                'manuallyApprovesFollowers': 'as:manuallyApprovesFollowers',
                'Hashtag': 'as:Hashtag',
            },
        ],
    });
});

app.get('/.well-known/nodeinfo', (req, res) => {
    res.json({
        links: [
            {
                rel: 'http://nodeinfo.diaspora.software/ns/schema/2.0',
                href: `${baseUrl}/api/v1/instance/nodeinfo/2.0`,
            }
        ],
    });
});

app.get('/api/v1/instance/nodeinfo/2.0', (req, res) => {
    res.json({
        version: '2.0',
        software: {
            name: 'funkwhale-faker',
            version: '0.0.0',
        },
        protocols: ['activitypub'],
        services: {
            inbound: [],
            outbound: [],
        },
        openRegistrations: false,
        usage: {
            users: {
                total: 0,
                activeHalfyear: 0,
                activeMonth: 0,
            },
        },
        metadata: {
            actorId: `${baseUrl}/federation/actors/service`,
            private: true,
            shortDescription: 'Funwhale Fake instance',
            longDescription: 'An self-written web server to try out federating with Funkwhale.',
            rules: '',
            contactEmail: '',
            terms: 'Private instance',
            nodeName: 'funkwhale-faker',
            banner: null,
            defaultUploadQuota: 100000,
            library: {
                federationEnabled: true,
                anonymousCanListen: true,
            },
            supportedUploadExtensions: [
                'aac',
                'aif',
                'aiff',
                'flac',
                'm4a',
                'mp3',
                'ogg',
                'opus',
            ],
            allowList: {
                enabled: false,
                domains: null,
            },
            reportTypes: [
                {
                    type: 'takedown_request',
                    label: 'Takedown request',
                    anonymous: true,
                },
                {
                    type: 'invalid_metadata',
                    label: 'Invalid metadata',
                    anonymous: false,
                },
                {
                    type: 'illegal_content',
                    label: 'Illegal content',
                    anonymous: true,
                },
                {
                    type: 'offensive_content',
                    label: 'Offensive content',
                    anonymous: false,
                },
                {
                    type: 'other',
                    label: 'Other',
                    anonymous: false,
                }
            ],
            funkwhaleSupportMessageEnabled: false,
            instanceSupportMessage: '',
            endpoints: {
                knownNodes: null,
                channels: `${baseUrl}/federation/index/channels`,
                libraries: `${baseUrl}/federation/index/libraries`,
            },
        },
    });
});

app.get('/federation/actors/service', (req, res) => {
    res.type('application/activity+json').json({
        id: `${baseUrl}/federation/actors/service`,
        outbox: `${baseUrl}/federation/actors/service/outbox`,
        inbox: `${baseUrl}/federation/actors/service/inbox`,
        preferredUsername: 'service',
        type: 'Service',
        name: 'service',
        followers: `${baseUrl}/federation/actors/service/followers`,
        following: `${baseUrl}/federation/actors/service/following`,
        manuallyApprovesFollowers: false,
        url: [
            {
                type: 'Link',
                href: `${baseUrl}/@service`,
                mediaType: 'text/html'
            }
        ],
        icon: null,
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1',
            'https://funkwhale.audio/ns',
            {
                manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
                Hashtag: 'as:Hashtag'
            }
        ],
        publicKey: {
            owner: `${baseUrl}/federation/actors/service`,
            publicKeyPem: '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3/ViLn6yN5mdG33ey4Ct\nK7gPwBhmxaLlYPUgnSSxctjYQi4493dLjpIyIQsVwAwFLPLvmkadozEDFES73t2r\nM40PDChffE7pETcpQ0WUKC4rVkkj23hoe8z1YM+YD86khq4g17X3TRWvxWgXwXJj\nWMKOs0ej2ivHpoYXNrEB+9klWrFmI2+yKM+L6WALuNTyJrbOPARmOkItlaFxYUoe\nDAu8BzETdLiYknbogbupfaATYw/hHpQwJxGJwwhFMjmcOA0xGMGXqoDyUZqSsh1w\n4x1tnhkFnwkqN7QolN4i8wayXOUhFR2W2uvQCQ2SowdVDi1paTefRzcYWIIgnmv2\nWQIDAQAB\n-----END PUBLIC KEY-----\n',
            id: `${baseUrl}/federation/actors/service#main-key`,
        },
        endpoints: {
            sharedInbox: `${baseUrl}/federation/shared/inbox`,
        },
    });
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
