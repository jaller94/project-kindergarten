const fs = require('fs');
const { createCanvas } = require('canvas');

const canvas = createCanvas(240, 160);
const ctx = canvas.getContext('2d');
ctx.fillRect(0, 0, 240, 160);

const out = fs.createWriteStream(__dirname + '/test.png');
const stream = canvas.createPNGStream();
stream.pipe(out)
out.on('finish', () =>  console.log('The PNG file was created.'));
