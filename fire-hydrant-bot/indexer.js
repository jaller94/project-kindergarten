'use strict';
const fs = require('fs');

const sharp = require('sharp');

const roundedCorners = new Buffer(
  '<svg><rect x="0" y="0" width="640" height="640" rx="60" ry="60"/></svg>'
);

const roundedCornerResizer =
  sharp()
    .resize(640, 640)
    .overlayWith(roundedCorners, { cutout: true });

function convert(folderName, fileName) {
  sharp(folderName + '/' + fileName)
    .resize(640, 640)
    .overlayWith(roundedCorners, { cutout: true })
    //.resize(1024, 1024)
    .toFile(`converted/${fileName}`, (err, info) => {
      if (err) {
        console.error(err);
      }
      if (info) {
        console.info(info);
      }
    });
}

fs.readdir('new', function(err, items) {
  console.log(items);
  for (const fileName of items) {
    convert('new', fileName);
  }
});
