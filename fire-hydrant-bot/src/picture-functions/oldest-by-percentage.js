'use strict';

const oldestByPercentage = (pictures, percentage) => {
    const sorted = pictures.sort((a, b) => (a.posted - b.posted));
    return sorted.slice(0, sorted.length * percentage);
}

module.exports = oldestByPercentage;
