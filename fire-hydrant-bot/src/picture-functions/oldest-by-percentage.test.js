const oldestByPercentage = require('./oldest-by-percentage.js');

describe('with a set of 3', () => {
    const oldest = {
        posted: new Date(2010, 0, 1),
    }
    const secondOldest = {
        posted: new Date(2011, 0, 1),
    }
    const pictures = [
        secondOldest,
        oldest,
        {
            posted: new Date(2013, 0, 1),
        },
    ];

    it('give two oldest with 70%', () => {
        const expected = [
            oldest,
            secondOldest,
        ];
        expect(oldestByPercentage(pictures, 0.7)).toEqual(expected);
    });
    it('give oldest with 40%', () => {
        const expected = [
            oldest,
        ];
        expect(oldestByPercentage(pictures, 0.4)).toEqual(expected);
    });
    it('give none out of 3 with 30%', () => {
        const expected = [];
        expect(oldestByPercentage(pictures, 0.3)).toEqual(expected);
    });
});
