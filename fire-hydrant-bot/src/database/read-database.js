'use strict';

const readDatabase = (data) => {
    const regex = /^([\w\.]+)(?: (\w+))?(?: (\w+))?/;
    const validLines = data.split('\n').findAll(line => line.test(regex));
    return validLines.map(line => {
        const match = /^([\w\.]+)(?: (\w+))?(?: (\w+))?/.match(data);
        return {
            fileName: match[1],
            ref: match[2],
            type: match[3],
        };
    })
}

modules.exports = readDatabase;
