'use strict';

const fs = require('fs');

const pickPicture = require('./post-generator/pick-picture.js');

function enrich(filePath) {
    if (Math.random() < 0.6) {
        return {
            filePath,
        }
    }
    return {
        filePath,
        posted: new Date(2018, Math.random()*12, Math.random()*28+1, Math.random()*24),
    }
}

fs.readdir('new', function(err, items) {
    const all_pictures = items.map(enrich);
    const picture = pickPicture(all_pictures);
    console.log(picture);
});
