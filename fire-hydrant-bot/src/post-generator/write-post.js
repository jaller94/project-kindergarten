'use strict';

const writePost = (picture) => {
    return `${picture.osm_node}
#VancouversFireHydrants #FireHydrantsPortraits
https://www.openstreetmap.org/node/${picture.osm_node}`;
}

module.exports = writePost;
