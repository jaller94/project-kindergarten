'use strict';

const oldestByPercentage = require('../picture-functions/oldest-by-percentage.js');

const randomFromArray = (arr) => {
    return arr[Math.floor((Math.random()*arr.length))];
}

const pickPicture = (all_pictures, percentage) => {
    let picture;
    const not_posted = all_pictures.filter(picture => !picture.posted);
    if (not_posted.length > 0) {
        picture = randomFromArray(not_posted);
    } else {
        const oldest = oldestByPercentage(all_pictures, 0.35);
        picture = randomFromArray(oldest);
    }
    return picture;
}

module.exports = pickPicture;
