'use strict';

const fs = require('fs');
const Mastodon = require('mastodon-api');

const data = fs.readFileSync('application-mastodon.social.json', 'utf8');
const config = JSON.parse(data);

console.log(config);
const baseUrl = 'https://mastodon.social';
const promise = Mastodon.getAuthorizationUrl(config.client_id, config.client_secret, baseUrl)
promise.then(console.log).catch(console.error);
