'use strict';
require('dotenv').config({ path: './.env-firehydrants' });
const Mastodon = require('mastodon-api');

const M = new Mastodon({
  access_token: process.env.ACCESS_TOKEN,
  api_url: process.env.API_URL,
});

M.get('timelines/tag/linux', {}).then(resp => console.log(resp.data[0]));
