'use strict';
require('dotenv').config({ path: './.env-firehydrants' });
const fs = require('fs');
const Mastodon = require('mastodon-api');

const M = new Mastodon({
  access_token: process.env.ACCESS_TOKEN,
  api_url: process.env.API_URL,
});

function getStatus(data) {
    let status = '';
    status += data.ref || 'unidentified fire hydrant';
    if (data.osmId) {
        status += `\nhttps://osm.org/node/${data.osmId}`;
    }
    if (data.tags) {
        status += '\n' + data.tags.map(tag => `#${tag}`).join(' ');
    }
    return status;
}

const data = {
    fileName: 'IMG_20180327_193451.jpg',
    ref: 'U18008',
    osmId: '5213343003',
    tags: [
        'firehydrant',
        'hydrantsOfVancouver',
        'hydrantsOfBC',
        'hydrantsOfCanada',
    ]
};

M.post('media', { file: fs.createReadStream(`converted/${data.fileName}`) }).then(resp => {
    const id = resp.data.id;
    M.post('statuses', { status: getStatus(data), media_ids: [id] })
});
