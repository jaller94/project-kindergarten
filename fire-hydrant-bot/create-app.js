'use strict';

const Mastodon = require('mastodon-api');

const url = 'https://mastodonten.de/api/v1/apps';
const clientName = 'Jaller\'s CLI poster';
const promise = Mastodon.createOAuthApp(url, clientName);
promise.then(console.log).catch(console.error);
