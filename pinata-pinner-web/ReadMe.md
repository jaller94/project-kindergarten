# Pinata Stick
_Gets the most out of your pinata!_

Pinata Stick is an **unofficial** webapp for the IPFS pinning service [pinata.cloud](https://pinata.cloud).

Demo video: [pinata-stick-2020-12-06-demo.webm (95.2 KiB)](https://gateway.pinata.cloud/ipfs/QmR2VBzxBX4QDFG342qgBc2tY9R7EioKmm3KJfFjsfm1kC?filename=pinata-stick-2020-12-06-demo.webm)

**This webapp currently uses unsecure localStorage to store your Pinata API credentials. Use it at your own risk!**

Demo (may take a (very) long time to load):\
https://k2k4r8n6p0ov2u9z6ib3qa7jwxzr8hi4l0u6ccgv7d4ojy7hw5cv974c.ipns.dweb.link/

## Requirements
* Free Pinata account
* JavaScript-enabled, modern webbrowser

## Features
* Static webapp (can be hosted on IPFS)
* Accessible (screen-reader-compatible, high contrast, simple English)
* [Progressive Web App](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps) can be installed on Android
* Share links to other Android apps
* Upload files
* View your pinned files
* Unpin files/folders
* Remembers your credentials
* No external code. Easy to audit.

## Planned features
* Service worker for offline usage
* Drag and drop upload of files
* Share files with the webapp on Android to upload them
* Simple search via metadata name
* Configurable IPFS gateway
* Display storage usage
* File previews

## Privacy and copyright considerations
**IPFS is a public file sharing network.** Please don't upload files you want to keep private. There's a chance that files can't be removed from the network. Others may willingly or unwillingly redistribute your files.

Only upload content with a [public copyright license](https://en.wikipedia.org/wiki/Public_copyright_license) or which is under [public domain](https://en.wikipedia.org/wiki/Public_domain). As a copyright holder, know that other people may unknowingly redistribute your content.

## Not on the roadmap
* Replication management
