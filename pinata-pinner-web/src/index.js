'use strict';

const defaults = {
    pinataApiKey: undefined,
    pinataSecretApiKey: undefined,
    ipfsPrefix: 'https://gateway.pinata.cloud/ipfs/',
};

const config = {
    ...defaults,
};

/**
 * Tests whether or not a user is logged in.
 * @returns {Promise<boolean>} true, if the the credentials work.
 */
async function isConnected(pinataApiKey, pinataSecretApiKey) {
    pinataApiKey = pinataApiKey || config.pinataApiKey;
    pinataSecretApiKey = pinataSecretApiKey || config.pinataSecretApiKey;
    if (!pinataApiKey || !pinataSecretApiKey) {
        return false;
    }
    try {
        const response = await fetch('https://api.pinata.cloud/data/testAuthentication', {
            method: 'GET',
            headers: {
                'pinata_api_key': pinataApiKey,
                'pinata_secret_api_key': pinataSecretApiKey,
            },
        });
        return response.status === 200;
    } catch (error) {
        console.warn(error);
        return false;
    }
}

const notify = (text) => {
    const notification = document.createElement('div');
    notification.innerText = text;
    document.getElementById('notifications').appendChild(notification);
    // Set a timeout to remove fade out the notification.
    setTimeout(() => {
        notification.classList.add('fade-out');
        // Remove it from the DOM, after it has been removed.
        setTimeout(() => {
            notification.remove();
        }, 2000);
    }, 12000);
};


async function unpin(hashToUnpin) {
    const response = await fetch(`https://api.pinata.cloud/pinning/unpin/${hashToUnpin}`, {
        method: 'DELETE',
        headers: {
            'pinata_api_key': config.pinataApiKey,
            'pinata_secret_api_key': config.pinataSecretApiKey,
        },
    });
    return response.status === 200;
    
}

function storeCredentials() {
    config.pinataApiKey = document.getElementById('api_key').value;
    config.pinataSecretApiKey = document.getElementById('secret_api_key').value;
    try {
    localStorage.setItem('api_key', config.pinataApiKey);
    localStorage.setItem('secret_api_key', config.pinataSecretApiKey);
    } catch (error) {
        console.warn('Failed to store credentials to localStorage');
        console.warn(error);
    }
}

async function uploadFile() {
    const { pinataApiKey, pinataSecretApiKey } = config;
    const uploadButton = document.getElementById('upload-form').querySelector('button');
    document.getElementById('url-output').value = 'Uploading…';
    uploadButton.disabled = true;
    const file = document.getElementById('file').files[0];
    if (!file) {
        notify('No file selected!');
        return;
    }
    const formData = new FormData();

    formData.append('file', file);
    console.dir(formData);
    if (!pinataApiKey || !pinataSecretApiKey) {
        notify('No credentials found!');
        return;
    }
    const response = await fetch('https://api.pinata.cloud/pinning/pinFileToIPFS', {
        method: 'POST',
        body: formData,
        headers: {
            // 'Content-Type': `multipart/form-data; boundary=${formData._boundary}`,
            'pinata_api_key': pinataApiKey,
            'pinata_secret_api_key': pinataSecretApiKey,
        },
    });
    if (response.status === 200) {
        const root = await response.json();
        const url = root.IpfsHash;
        // const ipfsHash = url.replace('https://gateway.pinata.cloud/ipfs/', '');
        document.getElementById('url-output').value = url;
        listPins();
    } else if (response.status === 413) {
        notify('File is too large!');
        document.getElementById('url-output').value = 'Error: File too large';
    } else {
        notify(`Pinata responded: HTTP Error ${response.status}`);
        document.getElementById('url-output').value = `Error ${response.status}`;
    }
    uploadButton.disabled = false;
}

async function listPins() {
    const pinataApiKey = localStorage.getItem('api_key');
    const pinataSecretApiKey = localStorage.getItem('secret_api_key');
    const response = await fetch('https://api.pinata.cloud/data/pinList?status=pinned&pageLimit=500', {
        method: 'GET',
        headers: {
            'pinata_api_key': pinataApiKey,
            'pinata_secret_api_key': pinataSecretApiKey,
        },
    });
    if (response.status !== 200) {
        return;
    }
    const root = await response.json();
    const searchList = document.getElementById('search-list');
    searchList.innerText = '';
    console.dir(root);
    for (const row of root.rows) {
        const url = `https://gateway.pinata.cloud/ipfs/${row.ipfs_pin_hash}`;
        const li = document.createElement('li');
        const name = document.createElement('a');
        name.innerText = row.metadata.name || 'Unnamed';
        name.setAttribute('href', url);
        name.setAttribute('target', '_blank');
        name.setAttribute('rel', 'noreferrer noopener');
        const shareButton = document.createElement('button');
        shareButton.type = 'button';
        shareButton.innerText = 'Share';
        shareButton.addEventListener('click', async (event) => {
            event.preventDefault();
            await share(url);
        });
        const unpinButton = document.createElement('button');
        unpinButton.type = 'button';
        unpinButton.innerText = 'Unpin';
        unpinButton.addEventListener('click', async (event) => {
            event.preventDefault();
            event.target.parentElement.classList.add('removed');
            event.target.disabled = true;
            try {
                await unpin(row.ipfs_pin_hash);
                event.target.parentElement.remove();
            } catch (error) {
                console.warn(error);
                notify('Failed to unpin.');
                event.target.parentElement.classList.remove('removed');
            }
        });
        li.append(name);
        li.append(shareButton);
        li.append(unpinButton);
        searchList.append(li);
    }
}

document.getElementById('login-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    await storeCredentials();
});
document.getElementById('upload-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    await uploadFile();
});
document.getElementById('test-login').addEventListener('click', async (event) => {
    event.preventDefault();
    notify(await isConnected() ? 'Successfully connected!' : 'Login failed!');
});

document.getElementById('search-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    await listPins();
});

async function shareHash(hash) {
    const url = `https://gateway.pinata.cloud/ipfs/${hash}`;
    if (typeof navigator.share === 'function') {
        share({ url });
    } else if (navigator.clipboard && typeof navigator.clipboard.writeText === 'function') {
        await navigator.clipboard.writeText(url);
    }
}

async function internalNavigate() {
    const loginPage = document.getElementById('login-page').style;
    const pinsPage = document.getElementById('pins-page').style;
    const uploadPage = document.getElementById('upload-page').style;
    if (location.hash.length > 0) {
        loginPage.display = location.hash === '#login' ? 'unset' : 'none';
        pinsPage.display = location.hash === '#pins' ? 'unset' : 'none';
        uploadPage.display = location.hash === '#upload' ? 'unset' : 'none';
    } else {
        const connected = await isConnected();
        pinsPage.display = 'none';
        uploadPage.display = connected ? 'unset' : 'none';
        loginPage.display = connected ? 'none' : 'unset';
    }
}

window.onhashchange = function (event) {
    event.preventDefault();
    internalNavigate();
}

async function init() {
    try {
        config.pinataApiKey = localStorage.getItem('api_key');
        config.pinataSecretApiKey = localStorage.getItem('secret_api_key');
        document.getElementById('api_key').value = config.pinataApiKey;
        document.getElementById('secret_api_key').value = config.pinataSecretApiKey;
    } catch (error) {
        console.warn('Failed to read credentials to localStorage');
        console.warn(error);
    }
    internalNavigate();
    if (await isConnected()) {
        listPins();
    }
}
init();
