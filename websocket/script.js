'use strict';

const playground = document.getElementById('playground');
const landNorth = document.getElementById('land-north');
const landSouth = document.getElementById('land-south');

let lastProgress = 0;
let progress = 0;


/* === Websocket Handling */
let socket = new WebSocket('ws://localhost:8001/', 'simplePaddleProtocol');

socket.onopen = function (event) {
    //TODO Initial data exchange
}

socket.onmessage = function (event) {
    try {
        progress = Number.parseInt(event.data);
    } catch (error) {
        console.error(error);
    }
}


/* === User Input Handling === */
window.addEventListener('beforeunload', function(){
    socket.close();
});

document.addEventListener('keydown', (event) => {
    if (event.key === ' ') {
        socket.send('f');
    };
});


/* === Game Loop */
function step(timestamp) {
    playground.style.backgroundPosition = -timestamp / 20 - progress + 'px 0';
    if (lastProgress !== progress) {
        landNorth.style.backgroundPosition = -progress + 'px 0';
        landSouth.style.backgroundPosition = -progress + 'px 0';
        lastProgress = progress;
    }
    window.requestAnimationFrame(step);
}

window.requestAnimationFrame(step);
