# Websocket demo
Simple multiplayer where you move a ship against a river.

## Server
1. Navigate into the server folder.
2. `yarn install`
3. `yarn start`

## Client
1. Open `index.html` in your browser.
2. Hold space to move forwards.
3. Connect more clients and hold space at the same time to move the ship faster.
