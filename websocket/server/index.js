'use strict';

const fs = require('fs');
const ws = require('nodejs-websocket');

let lastProgress = 0;
let progress = 0;

const options = {
    validProtocols: ['simplePaddleProtocol', 'jsonPaddleProtocol'],
};

var server = ws.createServer(options, function (conn) {
    console.log(conn);
    console.log('New connection');
    conn.sendText(progress);
    conn.on('text', function (str) {
        console.log('Received ' + str);
        if (str === 'f') {
            progress++;
        }
    });
    conn.on('close', function (code, reason) {
        console.log('Connection closed');
    });
    conn.onerror = function(event) {
        if (_websocket.readyState == 1) {
            console.log('ws normal error: ' + evt.type);
        }
    };
}).listen(8001);

function broadcast(server, msg) {
    server.connections.forEach(function (conn) {
        conn.sendText(msg);
    })
}

var count = 1;

setInterval(() => {
    if (lastProgress !== progress) {
        console.log('progress', progress);
        broadcast(server, progress.toString());
        lastProgress = progress;
    }
}, 50);
