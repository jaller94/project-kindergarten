import readline from 'readline';
import GameMaster from './game-master.mjs';

const gameMaster = new GameMaster();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: 'ToM> ',
});

rl.prompt();

rl.on('line', (line) => {
    switch (line.trim()) {
        case 'exit':
            shutDown();
            break;
        case 'start':
            startLoop();
            break;
        case 'step':
            step();
            break;
        default:
            console.log('Invalid command');
            break;
    }
    rl.prompt();
}).on('close', () => {
    shutDown();
});

function shutDown() {
    console.log('Have a great day!');
    process.exit(0);
}

var stepCount = 0;
async function step() {
    stepCount++;
    rl.setPrompt('');
    console.log(`Starting step ${stepCount}...`);
    rl.setPrompt('ToM> ');
    rl.prompt();
    try {
        gameMaster.step();
    } catch(err) {
        console.error(err);
        return 1;
    }
    return 0;
}

async function startLoop() {
    const error = await step();
    if (!error) setTimeout(() => {
        startLoop();
    }, 1000);
}
