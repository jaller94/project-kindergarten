import paper from 'paper/dist/paper-core';

const canvas = document.getElementById('map');
paper.setup(canvas);

var path;
// Draw the view now:
paper.view.draw();

const lineGroup = new paper.Group();

const generateGrid = (segments, lineWidth = 1) => {
  const width = 400;
  const height = 400;
  const distance = 400 / segments;
  const layer = new paper.Layer();
  for (let y = 1; y < segments; y++) {
    const path = new paper.Path();
    // Give the stroke a color
    path.strokeColor = 'rgba(255, 255, 255, 0.3)';
    path.strokeWidth = lineWidth;
    path.add(0, y * distance);
    path.add(width, y * distance);
    layer.addChild(path);
  }
  for (let x = 1; x < segments; x++) {
    const path = new paper.Path();
    // Give the stroke a color
    path.strokeColor = 'rgba(255, 255, 255, 0.3)';
    path.strokeWidth = lineWidth;
    path.add(x * distance, 0);
    path.add(x * distance, height);
    layer.addChild(path);
  }
  return layer;
};

const grids = [];
grids.push(generateGrid(10));
grids.push(generateGrid(40, 0.5));

const toggleGrids = () => {
  for (var grid of grids) {
    grid.visible = !grid.visible;
  }
};

const linePointerMove = (event) => {
  const lastX = path.segments[0].point.x;
  const lastY = path.segments[0].point.y;
  const vertical = Math.abs(lastX - event.clientX) > Math.abs(lastY - event.clientY);
  const x = vertical ? event.clientX : path.segments[0].point.x;
  const y = vertical ? path.segments[0].point.y : event.clientY;
  path.removeSegment(1);
  path.add([x, y]);
  // Draw the view now:
  paper.view.draw();
};

const linePointerUp = (event) => {
  paths.push(path);
  map.removeEventListener('pointermove', linePointerMove);
  window.removeEventListener('pointerup', linePointerUp);
};

const distance2D = (a, b) => {
  
};

const paths = [];

const findNearByPoint = (x, y) => {
  let bestDistance = Infinity;
  let point;
  for (const path of paths) {
    for (const segment of path.segments) {
      const distance = segment.point.getDistance(x, y);
      if (distance < bestDistance) {
        point = segment.point;
        bestDistance = distance;
      }
    }
  }
  console.log(bestDistance, point);
  if (bestDistance < 16) return point;
};

map.addEventListener('pointerdown', (event) => {
  const hits = lineGroup.hitTest([event.clientX, event.clientY]);
  if (!hits) {
    console.log(hits);
  }
  path = new paper.Path();
  lineGroup.addChild(path);
  // Give the stroke a color
  path.strokeColor = 'white';
  path.strokeWidth = 4;
  path.strokeCap = 'square';
  let point = findNearByPoint(event.clientX, event.clientY);
  if (!point) point = [event.clientX, event.clientY];
  path.add(point);
  // Draw the view now:
  paper.view.draw();
  
  map.addEventListener('pointermove', linePointerMove);
  window.addEventListener('pointerup', linePointerUp);
});

function downloadDataUri({ data, filename }) {
  const link = document.createElement("a");
  link.download = filename;
  link.href = data;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}

const button = document.getElementById('download');
button.addEventListener('click', () => {
  toggleGrids();
  paper.view.draw();
  const svg = paper.project.exportSVG({
    asString: true
  });
  toggleGrids();
  paper.view.draw();
  downloadDataUri({
    data: 'data:image/svg+xml;base64,' + btoa(svg),
    filename: 'floorplan.svg'
  });
});
button.disabled = false;

const grid = document.getElementById('grid');
grid.addEventListener('click', toggleGrids);
grid.disabled = false;
