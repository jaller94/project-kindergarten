var currentradio = 0;
var radio_on = false;
var radiostations = [
	{
		'name': 'WDR 2',
		'frequence': 89.7,
		'url': 'http://wdr-mp3-m-wdr2-koeln.akacast.akamaistream.net/7/812/119456/v1/gnl.akacast.akamaistream.net/wdr-mp3-m-wdr2-koeln'
	},
	{
		'name': '1Live diggi',
		'frequence': 89.7,
		'url': 'http://1live-diggi.akacast.akamaistream.net/7/965/119435/v1/gnl.akacast.akamaistream.net/1live-diggi'
	},
	{
		'name': 'Best Pony Radio',
		'frequence': 89.7,
		'url': 'http://104.131.25.97:8000/radio.mp3'
	},
];

function radioToogle() {
	if ( !radio_on ) {
		var html_radio = radioLoad( radiostations[currentradio] );
		html_radio.play();
		radio_on = true;
	} else {
		radioUnload();
		radio_on = false;
	}
}

function radioLoad(newStation) {
	var html_audiodiv = document.getElementById('audiodiv');
	var html_radio = document.createElement('audio');
	html_radio.id = 'radio';
	html_radio.src = newStation.url;
	html_radio.controls = 'controls'; //DEBUG

	html_audiodiv.appendChild( html_radio );

	return html_radio;
}

function radioUnload() {
	var html_audiodiv = document.getElementById('audiodiv');
	var html_radio = document.getElementById('radio');
	html_audiodiv.removeChild( html_radio );
}

function radioStartSwitch(newStation) {
	var html_radiostatic = document.getElementById('radiostatic');
	var html_radio = document.getElementById('radio');
	html_radiostatic.play();
	radioUnload();
	radioLoad( newStation );
	setTimeout( function() {radioStopSwitch()}, 500 );

	console.log('Switching to radio' + newStation.name + '!')
}

function radioStopSwitch() {
	var html_radio = document.getElementById('radio');
	var html_radiostatic = document.getElementById('radiostatic');
	if (html_radio.readyState) {
		html_radio.play();
		html_radiostatic.pause();
	} else {
		// audio isn't ready to play; add some delay
		setTimeout( function() {radioStopSwitch()}, 500 );		
	}
}

function radioUp() {
	currentradio = currentradio + 1;
	if (currentradio >= radiostations.length) {
		currentradio = 0;
	}
	radioStartSwitch( radiostations[currentradio] );
}

function radioDown() {
	currentradio = currentradio - 1;
	if (currentradio < 0) {
		currentradio = radiostations.length - 1;
	}
	radioStartSwitch( radiostations[currentradio] );
}
