'use strict';

var canvas = document.getElementById("car_canvas");
var ctx = canvas.getContext("2d");

var carAct = function( ) {
	for (var wheelname in this.wheels) {
		var wheelslot = this.wheels[wheelname];
		var wheel = wheelslot.wheel;
		if (wheel.speed > this.speed) {
			//console.log('Reifen wollen vorwärts!');
			//if (wheel.speed - this.speed < 20) {
				this.speed = wheel.speed;
			//} else {
				//console.log('Reifen dreht durch!');
			//}
		} else if (wheel.speed < this.speed) {
			//console.log('Reifen wollen rückwärts!');
			this.speed = this.speed + (wheel.speed - this.speed);
		}
		wheel.speed = wheel.speed * 0.99;
	}

	this.x = this.x + Math.sin(this.rot)*this.speed/30;
	this.y = this.y + -Math.cos(this.rot)*this.speed/30;
}

var carAccel = function( strength ) {
	var wheel = car.wheels['back'].wheel;
	wheel.speed = wheel.speed + strength * 0.5;
}

var carTurn = function( strength ) {
	this.rot = this.rot + strength/40;
}

var carHandbreak = function( strength ) {
	var wheel = car.wheels['front'].wheel;
	wheel.speed = 0;

	wheel = car.wheels['back'].wheel;
	wheel.speed = 0;
}

function newCar( color ) {
	var car = {
		x: 10,
		y: 10,
		rot: 0,
		speed: 0,
		width: 1.820,
		length: 4.769,
		color: color,
		wheels: {
			'front': {
				x: 0,
				y: 2
			},
			'back': {
				x: 0,
				y: -2
			}
		},

		act: carAct,
		accel: carAccel,
		turn: carTurn,
		handbreak: carHandbreak
	};
	car.wheels['front'].wheel = newFrontWheel();
	car.wheels['back'].wheel = newBackWheel();
	return car;
}

function newFrontWheel( color ) {
	var wheel = {
		direction: 0,
		speed: 0,
		rotation: 0,
		max_rotation: 0,
		min_rotation: 0,
		straight_drag: 10,
		side_drag: 20
	};
	return wheel;
}

function newBackWheel( color ) {
	var wheel = {
		direction: 0,
		speed: 0,
		rotation: 0,
		max_rotation: 0,
		min_rotation: 0,
		straight_drag: 0.95,
		side_drag: 0.95
	};
	return wheel;
}

var camera = 0;
var cars = [];

var car = newCar('red');
car.x = 2;
car.y = 7.5;
car.rot = Math.PI/2;
cars.push(car);

//var car2 = newCar('lightgreen');
//cars.push(car2);

function act() {
	controls();
	cars.forEach(function (car) {
		car.act();
	});
}

function controls() {
	if (keyStatus('keyup')) {
		car.accel(1);
	}
	if (keyStatus('keydown')) {
		car.accel(-1);
	}
	if (keyStatus('keyleft')) {
		car.turn(-1);
	}
	if (keyStatus('keyright')) {
		car.turn(1);
	}
	if (keyStatus('keyhandbreak')) {
		car.handbreak(1);
	}
}

function draw() {
	ctx.resetTransform();

	//ctx.rect(0, 0, 400, 400);
	//ctx.clip();

	ctx.fillStyle = 'gray';
	ctx.fillRect(0,0, 400,400);

	if (camera == 0) {
		ctx.scale(15,15);
		ctx.translate( -car.x + 10, -car.y + 10 );
	} else if (camera == 1) {
		ctx.scale(2,2);
	}

	road_cones.forEach(function (cone) {
		ctx.save()
		ctx.translate(cone.x, cone.y);
		ctx.fillStyle = 'orange';
		ctx.fillRect(-0.25,-0.25, 0.5,0.5);
		ctx.restore();
	});

	cars.forEach(function (car) {
		ctx.save()
		ctx.translate(car.x, car.y);
		ctx.rotate(car.rot);
		ctx.fillStyle = car.color;
		ctx.fillRect(-car.width/2,-car.length/2, car.width,car.length);
		ctx.fillStyle = 'lightgray';
		ctx.fillRect(-car.width*3/8,-car.length/3, car.width*3/4,car.length/6);
		ctx.restore();
	});

	ctx.resetTransform();
	ctx.fillStyle = "black";
	ctx.font="10px Courier";
	//ctx.fillText('Wheel: ' + car.wheels['back'].wheel.speed.toFixed(2) + 'm/s',0,370);
	ctx.fillText('Speed: ' + car.speed.toFixed(2) + 'm/s',0,380);
	ctx.fillText('Speed: ' + (car.speed * 3.6).toFixed(2) + 'km/h',0,390);

	requestAnimationFrame(draw);
}

var road_cones = [
	{x: 5, y: 5},
	{x: 10, y: 5},
	{x: 15, y: 5},
	{x: 20, y: 5},
	{x: 25, y: 5},
	{x: 30, y: 5},
	{x: 5, y: 10},
	{x: 10, y: 10},
	{x: 15, y: 10},
	{x: 20, y: 10},
	{x: 25, y: 10},
	{x: 30, y: 10}
];

var w = window;
requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

draw();
setInterval(function(){ act() }, 33);

bindStatus('keyup', 38);
bindStatus('keydown', 40);
bindStatus('keyleft', 37);
bindStatus('keyright', 39);
bindStatus('keyhandbreak', 32);

bindDown( function() {
	car.x = 10;
	car.y = 10;
}, 82);
bindDown( function() {
	camera = 0;
}, 49);
bindDown( function() {
	camera = 1;
}, 50);
