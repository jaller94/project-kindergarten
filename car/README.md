# Car
This is a car simulator video game. The player controls a vehicle in the top-down view.

The car is a rectangle with a front window. It has a radio which can be turned on and off by pressing P. The radio requires Internet to play any channels.

## How to start this?
Open `index.html` in a web browser. There are no building steps.

## Controls
### Car
* `up`/`down` — accelerate and break
* `left`/`right` — steer the car
* `P` — Turn the radio on and off
* `Page up`/`Page down` — Switch the radio channel

### Camera
* `1` — Close-up camera which follows you
* `2` — Far away static camera which looks at the spawn area
