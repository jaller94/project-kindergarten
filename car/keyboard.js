var binds_status = {};
var binds_up = [];
var binds_down = [];

function bindStatus( variable, keyCode ) {
	binds_status[variable] = {
		'keyCode': keyCode,
		'isDown': false
	};
}

function bindUp( func, keyCode ) {
	var bind = {
		'keyCode': keyCode,
		'function': func
	};
	binds_up.push( bind );
}

function bindDown( func, keyCode ) {
	var bind = {
		'keyCode': keyCode,
		'function': func
	};
	binds_down.push( bind );
}

function keyStatus( variable ) {
	return binds_status[variable].isDown;
}

function doKeyDown(e) {
	for (var name in binds_status) {
		var currentValue = binds_status[name];
		if (currentValue.keyCode === e.keyCode) {
			currentValue.isDown = true;
		}
	};

	binds_down.forEach(function(cur, index, arr) {
		if (cur.keyCode === e.keyCode) {
			cur.function();
		}
	});

	if ( e.keyCode === 17 ) { //Ctrl
		return false;
	} else if ( e.keyCode === 116 ) { //F5
		return false;
	} else {
		//alert( e.keyCode );
		return false;
	}
}

function doKeyUp(e) {
	for (var name in binds_status) {
		var currentValue = binds_status[name];
		if (currentValue.keyCode === e.keyCode) {
			currentValue.isDown = false;
		}
	};

	binds_up.forEach(function(cur, index, arr) {
		if (cur.keyCode === e.keyCode) {
			cur.function();
		}
	});

	if ( e.keyCode === 80 ) {
		radioToogle();
	} else if ( e.keyCode === 33 ) {
		radioUp();
	} else if ( e.keyCode === 34 ) {
		radioDown();
	}
}

window.addEventListener( "keydown", doKeyDown, false );
window.addEventListener( "keyup", doKeyUp, false );
