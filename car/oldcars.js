var carAct = function( ) {
	car.speed = car.speed + car.wheels['back'].speed;
	this.x = this.x + Math.sin(this.rot)*car.speed/30;
	this.y = this.y + -Math.cos(this.rot)*car.speed/30;
	this.speed = this.speed * 0.99;
}

var carAccel = function( strength ) {
	this.speed = this.speed + strength * 0.5;
}

var carTurn = function( strength ) {
	this.rot = this.rot + strength/40;
}

var carHandbreak = function( strength ) {
	this.speed = this.speed * 0.86;
}