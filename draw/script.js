'use strict';

class Pointer {
    configureContext() {
        context.lineWidth = this.lineWidth || Pointer.lineWidth;
        context.strokeColor = this.getColor();
    }

    getColor() {
        return this.color || Pointer.black;
    }

    setColor(color) {
        this.color = color;
    }
}

Pointer.prototype.lineWidth = 3;
Pointer.prototype.color = 'black';

function startDrawing(e) {
    isDrawing = true;
    context.beginPath();
    context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
    if (!pointers[e.pointerId]) {
        pointers[e.pointerId] = newPointer(e.pointerId, e);
    }
}

function stopDrawing(e) {
    isDrawing = false;
}

function draw(e) {
    if (isDrawing === true) {
        if (e.pointerId !== currentPointer) {
            pointers[e.pointerId].configureContext(context);
        }

        const x = e.pageX - canvas.offsetLeft;
        const y = e.pageY - canvas.offsetTop;
        context.lineTo(x, y);
        context.stroke();
    }
}

function store() {
    const target = document.getElementById('storage');
    target.src = canvas.toDataURL();
}

function save() {
    const imageData = context.getImageData(0, 0, 100, 50);
    const arrayOfPixels = imageData.data;
    // 0-r 1-g 2-b 3-a
    context.putImageData(imageData, 100, 0);
}

function clearCanvas() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function newPointer(id, event) {
    const pointer = new Pointer();
    pointer.setColor(colors[id]);
    return pointer;
}

var colors = 'black,red,gold,green,blue,pink,orange,purple'.split(',');
var pointers = [];
var currentPointer = null;

var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var isDrawing = false;

canvas.onpointerdown = startDrawing;
canvas.onpointerup = stopDrawing;
//canvas.onmouseout = stopDrawing;
canvas.onpointermove = draw;

document.getElementById('store').onclick = store;
document.getElementById('save').onclick = save;

console.log(`Let's draw!`);
