function drop(e) {
    e.stopPropagation();
    e.preventDefault();

    const data = e.dataTransfer;
    const files = data.files;

    for (const file of files) {
        processFile(file);
    }
}

function ignoreDrag() {
    // Make sure nobody else gets this event, because you're handling
    // the drag and drop.
    e.stopPropagation();
    e.preventDefault();
}

var dropBox;
dropBox = document.getElementById('dropBox');
dropBox.ondragcenter = ignoreDrag;
dropBox.ondragover = ignoreDrag;
dropBox.ondrop = drop;
