'use strict';

const body = document.getElementsByTagName('body')[0];
const display = document.getElementById('display');
const display_correct = document.getElementById('correct');
const display_incorrect = document.getElementById('incorrect');
const input = document.getElementById('input');
const value_seconds = document.getElementById('value_seconds');
const value_completed = document.getElementById('value_completed');
const value_correct_keys_ratio = document.getElementById('value_correct_keys_ratio');
const value_chars_per_second = document.getElementById('value_chars_per_second');

let myText;

const stats = {
	completed: 0,
	corret_keys: 0,
	all_keys: 0,
	startTime: new Date()
};

const words = [
	'magically delicious',
	'a critical amount of black matter',
	'I appreciate your company.',
	'appropriate ape',
	'I fail to comprehend why you did this.',
	'I send it in an envelope.',
	'to lose a game',
	'prefer',
	'a substitute for sugar',
	'vowel',
	'She worked very diligently.',
	'feedback preferred',
	'preferably good',
	'His eagerness was extraordinary.'
];

function newString() {
	return words[Math.floor(Math.random()*words.length)];;
}

function newGame() {
	if (myText) {
		recalcStats();
	}
	myText = newString();
	display_correct.textContent = '';
	display_incorrect.textContent = myText;
	input.textContent = '';
}

function recalcStats() {
	// Update variables
	stats.completed ++;
	stats.corret_keys += myText.length;

	// Update interface
	value_completed.textContent = stats.completed;
	let ratio = ((stats.corret_keys / stats.all_keys) * 100).toFixed(3) + ' %';
	value_correct_keys_ratio.textContent = ratio;
	// time difference in minutes
	let timeDiff = (new Date().getTime() - stats.startTime.getTime()) / 1000 / 60;

	value_chars_per_second.textContent = stats.corret_keys / timeDiff;
}

function checkGame() {
	if (input.textContent === myText) {
		newGame();
	}

	if (myText.startsWith(input.textContent)) {
		display_correct.textContent = myText.substr(0, input.textContent.length);
		body.classList.remove('error');
	} else {
		body.classList.add('error');
	}
}

// create an observer instance
var observer = new MutationObserver(function(mutations) {
	console.log(mutations);
	stats.all_keys++;
	checkGame();
});

// configuration of the observer:
var config = { attributes: true, childList: true, subtree: true, characterData: true };

// pass in the target node, as well as the observer options
observer.observe(input, config);

newGame();
