require('dotenv').config();
const Mastodon = require('mastodon-api');

const M = new Mastodon({
    access_token: process.env.ACCESS_TOKEN,
    timeout_ms: 60*1000,  // optional HTTP request timeout to apply to all requests.
    api_url: 'https://mastodonten.de/api/v1/', // optional, defaults to https://mastodon.social/api/v1/
});

M.get('accounts/verify_credentials', {}).then(resp => {
    //console.log(resp.data);
    M.get(`accounts/${resp.data.id}/statuses`, {limit: 40}).then(resp => {
        let statuses = resp.data;
        M.get(`accounts/${resp.data.id}/statuses`, {limit: 40, max_id: statuses[39]}).then(resp => {
            statuses = [
                ...statuses,
                resp.data,
            ];
            for (const status of statuses) {
                console.log(status.reblogs_count, status.favourites_count, (status.content || '').substring(0,150));
            }
        });
    }).catch(error => {
        console.log(error);
    });
}).catch(error => {
    console.log(error);
});
