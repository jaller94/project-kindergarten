const fsPromises = require('fs').promises;

async function main() {
    const arr = [];
    const files = await fsPromises.readdir('.');
    for (const file of files) {
        if (!/\.json$/.test(file)) continue;
        const data = await fsPromises.readFile(`./${file}`, 'utf8');
        const item = JSON.parse(data);
        const date = new Date(item.taken_at_timestamp * 1000);
        const likes = (item.edge_media_preview_like || {}).count ?? 0;
        const comments = (item.edge_media_preview_comment || {}).count ?? 0;
        let text = '';
        try {
            text = item.edge_media_to_caption.edges[0].node.text;
        } catch { }
        text = text.normalize();
        const hashtags = text.match(/#[\wäöüß]+/ig) || [];
        arr.push({
            name: file,
            timestamp: date,
            likes,
            comments,
            hours: date.getHours(),
            month: date.getMonth() + 1, // JavaScript's months are zero-based
            year: date.getFullYear(),
            dayOfMonth: date.getDate(),
            dayOfWeek: date.getDay(),
            hashtags,
        });
    }
    fsPromises.writeFile('data.json', JSON.stringify(arr, null, 2), 'utf8');
}

main().catch(console.error);
