# Data analysis tool

I use this tool to analyse Instagram reactions.

![](./docs/preview.png)

## Data extraction

1. Install `instalooter`.
1. Run `instalooter user <profile> --get-videos --dump-json` to download all posts.
1. Run `node extractor.js` to extract the information into `data.json`.

## Hosting

This project can be hosted on a static webspace.

For local development you may run `python -m http.server`.
