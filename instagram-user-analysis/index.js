'use strict';

function formatDate(date) {
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const mins = String(date.getMinutes()).padStart(2, '0');
    return `${date.getDate()}.${month}.${date.getFullYear()} ${hours}:${mins}`;
}

function a(data, width, heightProp = d => d.likes) {
    const x = d3.scaleLinear()
        .domain([0, d3.max(data.map(heightProp))])
        .range([0, width - 100]);

    const y = d3.scaleBand()
        .domain(d3.range(data.length))
        .range([0, 15 * data.length]);

    const svg = d3.create("svg")
        .attr("width", width)
        .attr("height", y.range()[1])
        .attr("font-family", "sans-serif")
        .attr("font-size", "10")
        .attr("text-anchor", "end");

    const bar = svg.selectAll("g")
        .data(data)
        .join("g")
        .attr("transform", (d, i) => `translate(0,${y(i)})`);

    bar.append("rect")
        .attr("fill", "steelblue")
        .attr("width", d => x(heightProp(d)))
        .attr("height", y.bandwidth() - 1);

    bar.append("text")
        .attr("fill", "white")
        .attr("x", d => x(heightProp(d)) - 3)
        .attr("y", y.bandwidth() / 2)
        .attr("dy", "0.35em")
        .text(heightProp);

    bar.append("text")
        .attr("fill", "black")
        .attr("x", width)
        .attr("y", y.bandwidth() / 2)
        .attr("dy", "0.35em")
        .text(d => formatDate(new Date(d.timestamp)));

    return svg.node();
}

async function main() {
    const originalData = await d3.json('data.json');

    window.redraw = () => {
        const chart = document.getElementById('chart');

        const prop = document.getElementById('height-prop-input').value;
        let propFunc = d => d[prop];
        if (prop === 'hashtags') {
            propFunc = d => d.hashtags.length;
        }

        const sortBy = document.getElementById('sort-by-input').value;
        const descending = !document.getElementById('sort-order-input').checked;
        let filteredData = [...originalData];

        if (sortBy === 'timestamp') {
            filteredData.sort((a, b) => (new Date(a.timestamp)).getTime() - (new Date(b.timestamp)).getTime());
        } else if (sortBy === 'likes') {
            filteredData.sort((a, b) => a.likes - b.likes);
        } else if (sortBy === 'comments') {
            filteredData.sort((a, b) => a.comments - b.comments);
        } else if (sortBy === 'hashtags') {
            filteredData.sort((a, b) => a.hashtags.length - b.hashtags.length);
        }

        if (descending) {
            filteredData.reverse();
        }

        let limit;
        if (document.getElementById('top-20-input').checked) {
            limit = 20;
        }
        if (limit) {
            filteredData = filteredData.slice(0, limit);
        }

        let maxAge;
        if (document.getElementById('last-12-months-input').checked) {
            maxAge = new Date();
            maxAge.setFullYear(maxAge.getFullYear() - 1);
        }
        if (maxAge) {
            filteredData = filteredData.filter(d => new Date(d.timestamp).getTime() >= maxAge.getTime())
        }

        chart.innerHTML = '';
        const div = document.createElement('div');
        div.innerText = `Entries: ${filteredData.length}. Total: ${filteredData.reduce((prev, curr) => prev + propFunc(curr), 0)}`;
        chart.append(div);
        chart.append(a(filteredData, 600, propFunc));
    };

    redraw('likes');
}

main();
