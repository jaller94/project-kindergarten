import { describe, expect, test } from 'bun:test';
import { ParseError, Transaction, parseTransaction, resolveTransactions } from './pocket';

describe('parseTransaction', () => {
    test('parses an integer amount', () => {
        const input = 'Alice owes Bob 20';
        const actual = parseTransaction(input);
        expect(actual).toEqual({
            debtor: 'Alice',
            debtee: 'Bob',
            amount: 20,
            raw: input,
            note: '',
        });
    });
    test('parses a negative integer amount', () => {
        const input = 'Alice owes Bob -20';
        const actual = parseTransaction(input);
        expect(actual).toEqual({
            debtor: 'Alice',
            debtee: 'Bob',
            amount: -20,
            raw: input,
            note: '',
        });
    });
    test('parses an float amount', () => {
        const input = 'Alice owes Bob 102.56';
        const actual = parseTransaction(input);
        expect(actual).toEqual({
            debtor: 'Alice',
            debtee: 'Bob',
            amount: 102.56,
            raw: input,
            note: '',
        });
    });
    test('parses a negative float amount', () => {
        const input = 'Alice owes Bob -3.2';
        const actual = parseTransaction(input);
        expect(actual).toEqual({
            debtor: 'Alice',
            debtee: 'Bob',
            amount: -3.2,
            raw: input,
            note: '',
        });
    });
    test('rejects the amount 0', () => {
        const input = 'Alice owes Bob 0';
        const actual = parseTransaction(input);
        expect(actual).toBe(ParseError.INVALID_AMOUNT);
    });
    test('rejects the amount 0.0', () => {
        const input = 'Alice owes Bob 0.0';
        const actual = parseTransaction(input);
        expect(actual).toBe(ParseError.INVALID_AMOUNT);
    });
    test('rejects if the amount is missing', () => {
        const input = 'Alice owes Bob ';
        const actual = parseTransaction(input);
        expect(actual).toBe(ParseError.INVALID_FORMAT);
    });
});

describe('everything', () => {
    test('A', () => {
        const input = 'Alice owes Bob 10\nEveryone owes Bob 9\nChris owes Bob 4';
        const parsed = input.split('\n').map(parseTransaction);
        const transactions: Transaction[] = [];
        for (const transaction of parsed) {
            if (typeof transaction !== 'object') {
                throw Error();
            }
            transactions.push(transaction);
        }
        const actual = resolveTransactions(transactions, true);
        expect(actual).toEqual(new Map([
            ['Alice', -13],
            ['Bob', 20],
            ['Chris', -7],
        ]));
    });
});
