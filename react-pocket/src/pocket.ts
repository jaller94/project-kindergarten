export const parseMoney = (str): number => {
  str = str.replace(/,/, '').replace(/[$€ ]/, '');
  const float = Number.parseFloat(str);
  if (Number.isNaN(float)) return NaN;
  return Number.parseFloat(float.toFixed(2));
}

export enum ParseError {
  INVALID_FORMAT,
  INVALID_DEBTOR,
  INVALID_DEBTEE,
  INVALID_AMOUNT,
}

export const parseTransaction = (raw: string): Transaction | undefined | ParseError => {
  const match = raw.match(/(\w+) owes? (\w+) ([$€ \d,.-]+)(.*)/);
  if (!match) {
    return ParseError.INVALID_FORMAT;
  }
  const debtor = match[1];
  const debtee = match[2];
  const amount = parseMoney(match[3]);
  const note = match[4].trim();

  if (!debtor) return ParseError.INVALID_DEBTOR;
  if (!debtee) return ParseError.INVALID_DEBTEE;
  if (typeof amount !== 'number' || Number.isNaN(amount) || !Number.isFinite(amount)) return ParseError.INVALID_AMOUNT;
  if (amount === 0) return ParseError.INVALID_AMOUNT;

  const transaction: Transaction = {
    raw,
    debtor,
    debtee,
    amount,
    note,
  };

  return transaction;
}

export type Transaction = {
  raw: string,
  debtor: string,
  debtee: string,
  amount: number,
  note: string,
  currency?: string,
};

export const resolveTransactions = (transactions: Transaction[], merge = false, groups = new Map<string, string[]>()) => {
  const balances = new Map<string, number>();
  for (const transaction of transactions) {
    balances.set(transaction.debtor, (balances.get(transaction.debtor) ?? 0) - transaction.amount);
    balances.set(transaction.debtee, (balances.get(transaction.debtee) ?? 0) + transaction.amount);
  }

  if (merge) {
    // Auto group: Everyone
    if (!groups.has('Everyone')) {
      const everyone: string[] = [];
      for (const key of balances.keys()) {
        if (key !== 'Everyone' && !groups.has(key)) {
          everyone.push(key);
        }
      }
      groups.set('Everyone', everyone);
    }

    for (const [key, members] of groups) {
      for (const member of members) {
        balances.set(member, (balances.get(member) ?? 0) + (balances.get(key) ?? 0) / members.length);
      }
      balances.delete(key);      
    }
  }

  console.debug(balances);

  return balances;
}
