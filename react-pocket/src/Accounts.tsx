import React from 'react';

export default function Accounts({balances}) {
  const listItems = [];
  for (const [key, balance] of balances) {
    listItems.push(
      <li key={key}>{key} {balance < 0 ? 'needs to pay' : 'receives'} {Math.abs(balance).toFixed(2)} €</li>
    );
  }
  return (
    <ul>{listItems}</ul>
  );
}
