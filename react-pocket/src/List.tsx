import React from 'react';
import './List.css';

export default function List(props) {
  const entries = props.entries;
  const listItems = entries.map((entry) =>
    <tr key={entry.eventId}>
      <td>{entry.debtor}</td>
      <td>{entry.debtee}</td>
      <td>{entry.amount.toFixed(2)} €</td>
      <td>{entry.category}</td>
    </tr>
  );
  return (
    <table className="List">
      <thead>
        <tr>
          <td>Debtor</td>
          <td>Debtee</td>
          <td>Amount</td>
          <td>Comment</td>
        </tr>
      </thead>
      <tbody>
        {listItems}
      </tbody>
    </table>
  );
}
