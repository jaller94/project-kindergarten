import React, { Component } from 'react';
import Accounts from './Accounts.js';
import List from './List.js';
import { parseTransaction, resolveTransactions } from './pocket.js';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      balances: new Map(),
      entries: [],
    };
  }

  parseEntries(log) {
    const entries = [];
    let i = 1;
    for (const line of log.split('\n')) {
      if (line.trim() === '') {
        continue;
      }
      const owe = parseTransaction(line, i);
      if (!owe || owe instanceof Error) {
        console.warn('Unable to parse line!', line);
        return [];
      }
      entries.push(owe);
      i++;
    }
    console.debug('entries', entries);
    return entries;
  }

  // parseGroups() {
  //   const groups = new Map();
  //   return groups;
  // }

  handleChange = (event) => {
    console.log('== Calculation restarted ==');
    const entries = this.parseEntries(event.target.value);
    // const groups = this.parseGroups(event.target.value);
    const balances = resolveTransactions(entries, true);
    this.setState({balances, entries});
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Pocket</h1>
        </header>
        <p className="App-intro">
          <textarea
            className="App-pocket-input"
            onChange={this.handleChange}
          >
          </textarea>
        </p>
        <Accounts
          balances={this.state.balances}
        />
        <List
          entries={this.state.entries}
        />
      </div>
    );
  }
}

export default App;
