# Blocks
This was supposed to become an HTML clone of the Android game [1010! Block Puzzle Game](https://play.google.com/store/apps/details?id=com.gramgames.tenten).

You are presented with three Tetris-like block formations which you have to position in a 10x10 grid. Then you are presented the next three block formations and so on. Blocks cannot be overlapped. Rows and columns of blocks will be cleared when they got filled. The game ends when there is no solution left to place the new blocks.
