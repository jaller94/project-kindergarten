# Project Kindergarten
My name is Christian and I am a web developer. This repository is where most of my ideas grow up. As small, unpolished — and often not working — prototypes.

Every folder contains something different, for example:
* a technology I’ve been trying,
* a video game I came up with,
* a useful webapp,
* a crazy new space story told by chatbots using procedural generation and AI.

Most of these were one-shot ideas and they are no longer maintained. Be aware that outdated packages from NPM might have known vulnerabilities. I suggest you blindly update the dependencies and look if it still works.

If you find something cool, send me a message, an issue or a PR. I am happy about every interaction with my prototypes!
