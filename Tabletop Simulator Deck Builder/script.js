const CARDS_X = 10;
const CARDS_Y = 7;

const canvas = document.getElementById('main');
const ctx = canvas.getContext('2d', { alpha: false});

const cards = [];

function drawCard(id, dx, dy, dWidth, dHeight) {
    if (cards[id]) {
        ctx.drawImage(cards[id], dx, dy, dWidth, dHeight);
    }
}

function drawCards(...ids) {
    const width = canvas.width / CARDS_X;
    const height = canvas.height / CARDS_Y;
    for (let y = 0; y <= CARDS_Y; y++) {
        for (let x = 0; x <= CARDS_X; x++) {
            const id = x + y * CARDS_X;
            if (ids.length && !ids.includes(id)) {
                continue;
            }
            drawCard(id, x * width, y * height, width, height);
        }
    }
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawCards();
}

function resizeCanvas() {
    //canvas.width = 750 * CARDS_X * 0.5;
    //canvas.height = 1050 * CARDS_Y * 0.5;
    canvas.width = 4096;
    canvas.height = 4096;
}

// window.addEventListener('resize', function (event) {
//     resizeCanvas();
//     draw();
// });

document.addEventListener('drop', function(e) {
    e.stopPropagation();
    e.preventDefault();
    console.log('received drop');

    var files = e.dataTransfer.files; // Array of all files

    for (const file of files) {
        if (file.type.startsWith('image/')) {
            const reader = new FileReader();
            const image = new Image();
            const id = cards.push(image) - 1;

            reader.onload = function(e2) {
                image.onload = function() {
                    drawCards(id);
                }
                image.src = e2.target.result;
            }

            reader.readAsDataURL(file); // start reading the file data.
        }
    }
}, false);

function positionToId(x, y) {
    const width = canvas.width / CARDS_X;
    const height = canvas.height / CARDS_Y;
    return Math.floor(x / width) + Math.floor(y / height) * CARDS_X;
}

function swapCards(id1, id2) {
    const tmp = cards[id1];
    cards[id1] = cards[id2];
    cards[id2] = tmp;
}

let draggedCard;

canvas.addEventListener('mousedown', event => {
    const id = positionToId(event.clientX, event.clientY);
    console.log(id, event.clientX, event.clientY);
    draggedCard = id;
});

canvas.addEventListener('mouseup', event => {
    const id = positionToId(event.clientX, event.clientY);
    console.log(id, event.clientX, event.clientY);
    swapCards(draggedCard, id);
    drawCards(draggedCard, id);
    draggedCard = undefined;
});

function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

document.addEventListener('dragover', handleDragOver, false);
document.getElementById('redraw').addEventListener('click', function() {
    window.requestAnimationFrame(draw);
});

resizeCanvas();

window.requestAnimationFrame(draw);
