const canvas = document.getElementById('canvas');
const tilesheet = document.getElementById('tilesheet');
canvas.width = 640;
canvas.height = 360;
const ctx = canvas.getContext('2d');

function generateChunk() {
  const array = [];
  for (let i = 0; i < 256; i++) {
    array.push();
  }
  return array;
}

function generateMap(globalX, globalY, seed = 0) {
  const array = generateChunk();
  for (let y = 0; y < 16; y++) {
    for (let x = 0; x < 16; x++) {
      const value = Math.sin(globalX + x * 1) + Math.sin(globalY + y * 1);
      array[x + y * 16] = value > 0 ? 1 : 0;
    }
  }
  return array;
}

function drawChunk(ctx, chunk) {
  for (let y = 0; y < 16; y++) {
    for (let x = 0; x < 16; x++) {
      const tile = chunk[x + y * 16];
      const xPos = tile * 16 % tilesheet.width;
      const yPos = Math.floor(tile * 16 / tilesheet.width) * 16;
      ctx.drawImage(tilesheet, xPos, yPos, 16, 16, x*16, y*16, 16, 16);
    }
  }
}

const chunks = [];
for (let y = 0; y < 4; y++) {
  const row = [];
  chunks.push(row);
  for (let x = 0; x < 4; x++) {
    row.push(generateMap(x, y));
  }
}

function redrawWorld() {
  ctx.fillStyle = '#472d3c';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  chunks.forEach((chunkRow, y) => {
    if (chunkRow) {
      chunkRow.forEach((chunk, x) => {
        ctx.translate(x * 256, y * 256);
        drawChunk(ctx, chunk, 0, 0);
        ctx.setTransform(1, 0, 0, 1, 0, 0);
      });
    }
  });
}

window.addEventListener('load', () => {
  redrawWorld();
  ctx.strokeText('Hello world!', 0, 30);
});

let paint = 0;

tilesheet.addEventListener('click', (event) => {
  const xTile = Math.floor(event.offsetX / 16);
  const yTile = Math.floor(event.offsetY / 16);
  const tileId = xTile + (yTile * tilesheet.width / 16);
  // alert(tileId);
  paint = tileId;
});

canvas.addEventListener('click', (event) => {
  const xChunk = Math.floor(event.offsetX / 256);
  const yChunk = Math.floor(event.offsetY / 256);
  const xTile = Math.floor(event.offsetX % 256 / 16);
  const yTile = Math.floor(event.offsetY % 256 / 16);
  const chunk = chunks[yChunk][xChunk];
  const tilePosition = xTile + yTile * 16;
  console.log(xChunk, yChunk, tilePosition);
  chunk[tilePosition] = paint;
  redrawWorld();
});
 