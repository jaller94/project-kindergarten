"use strict";

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

class Person {
	constructor(map, x=50, y=50, left=0.5) {
		this.map = map;
		this.x = x;
		this.y = y;
		this.left = 1;
		this.variance = 0.2;
	}

	act() {
		this.x += Math.random()-0.5;
		this.y += Math.random()-0.5;
		this.left += (Math.random()-0.5) * 0.3;

		const neighbor = map.getRandomPerson();
		const diff = Math.abs(this.left - neighbor.left);
		const lefter = (this.left > neighbor.left);
		if (diff > 0.4) {
			this.left += (lefter ? 0.1 : -0.1);
		} else {
			this.left -= (lefter ? 0.1 : -0.1);
		}

		this.x = Math.max(Math.min(this.x, map.width), 0);
		this.y = Math.max(Math.min(this.y, map.height), 0);
		this.left = Math.max(Math.min(this.left, 1), 0);
	}

	getColor() {
		const red = Math.floor(this.left * 255);
		return 'rgb('+red+',0,0)';
	}

	distanceTo(x, y) {
		const dx = x-this.x;
		const dy = y-this.y;
		return Math.sqrt(dx*dx + dy*dy);
	}

	distanceTo(person) {
		return this.distanceTo(person.x, person.y);
	}
}

class Map {
	constructor() {
		this.width = 99;
		this.height = 99;
		this.persons = [];
	}

	act() {
		for (const person of this.persons) {
			person.act();
		}
	}

	getRandomPerson() {
		if (this.persons.length === 0) return null;

		const index = getRandomInt(0, this.persons.length);
		return this.persons[index];
	}
}
