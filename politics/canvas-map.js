"use strict";

const canvas = document.getElementById('canvas-map');
const ctx = canvas.getContext('2d');

function drawPeople(map) {
	for (const person of map.persons) {
		const x = person.x * 5;
		const y = person.y * 5;

		ctx.fillStyle = person.getColor();
		ctx.fillRect(x-1, y-1, 3, 3);
	}
}

function draw() {
	const canvas = ctx.canvas;

	ctx.fillStyle = "white";
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	if (window.map) {
		drawPeople(window.map);
	} else {
		ctx.fillStyle = "green";
		ctx.fillText('Map not loaded!', 10, 50);
	}

	requestAnimationFrame(draw);
}

requestAnimationFrame(draw);
