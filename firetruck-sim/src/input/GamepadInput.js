import Victor from 'victor';

export default class ControllerInput {
    constructor(gamepad) {
        this.gamepad = gamepad;
    }

    movement() {
        return Victor(this.gamepad.axes[0], this.gamepad.axes[1]);
    }

    target() {
        if (this.gamepad.id === '046d-c20d-Logitech WingMan Attack 2') {
            return Victor(10, 0).rotateDeg(-this.gamepad.axes[2] * 180 - 90);
        }
        return Victor(this.gamepad.axes[3], this.gamepad.axes[4]);
    }
}
