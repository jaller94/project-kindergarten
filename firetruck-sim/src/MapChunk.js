export const createMapChunk = () => {
    const mapchunk = [];
    for (let y = 0; y < 64; y++) {
        const row = [];
        for (let x = 0; x < 64; x++) {
            const items = [1,2,3,4,5,6,7,8,9,10,11,12];
            const randItem = items[Math.floor(Math.random()*items.length)];
            row.push(randItem);
        }
        mapchunk.push(row);
    }
    return mapchunk;
};
