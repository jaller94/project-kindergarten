import Victor from 'victor';

export default class PlayerActor {
    constructor() {
        this._ang = 0;
        this._pos = new Victor(100, 100);
    }

    ang() {
        return this._ang;
    }

    pos() {
        return this._pos.clone();
    }

    move(vector, delta) {
        const speed = 14;
        if (vector.lengthSq() > 1) {
            vector = vector.normalize();
        }
        vector = vector.limit(0, speed * delta);
        this._pos.add(vector);
    }

    setAngle(angle) {
        this._ang = angle;
    }

    setPosition(position) {
        this._pos = position.clone();
    }
}
