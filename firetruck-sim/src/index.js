import PlayerActor from './PlayerActor.js';
import GamepadInput from './input/GamepadInput.js';
import {createMapChunk} from './MapChunk.js';

function getNodeById(elements, id) {
    return elements.find(element => element.id === id);
}

const buildings = [];
const fireHydrants = [];

function isBuilding(element) {
    return (
        element.type === 'way'
        && element.tags
        && element.tags.building
    );
}

function isFireHydrant(element) {
    if (element.type === 'node') console.dir(element.tags);
    return (
        element.type === 'node'
        && element.tags
        && element.tags.emergency === 'fire_hydrant'
    );
}

function processDownload(elements) {
    for (const element of elements) {
        if (isBuilding(element)) {
            const building = [];
            for (const nodeId of element.nodes) {
                const node = getNodeById(elements, nodeId);
                if (!node) console.error(`Could not find node ${nodeId}`);
                building.push(node);
            }
            buildings.push(building);
        }
        if (isFireHydrant(element)) {
            console.log('yay');
            fireHydrants.push(element);
        }
    }
    console.dir(elements);
}

fetch('/export.json').then((response) => {
    return response.json();
}).then((jsonResponse) => {
    processDownload(jsonResponse.elements);
    console.dir(buildings);
    console.dir(fireHydrants);
}).catch((err) => {
    console.error(err);
});

function startGame() {
    window.requestAnimationFrame(step);
    drawLoop();
}

import ImageLoader from './ImageLoader.js';

const loader = new ImageLoader(startGame);

const firetruckImg = loader.loadImage('/gfx/firetruck.png');

const canvas = document.getElementById('main');
const ctx = canvas.getContext('2d');

function fixPosition({x, y}) {
    const inMinY = 49.24417110390574;
    const inMinX = -123.04287314414978;
    const inMaxY = 49.24622328576655;
    const inMaxX = -123.03819537162781;
    const outMinX = 0;
    const outMinY = 600;
    const outMaxX = 1300;
    const outMaxY = 0;
    x = (x - inMinX) / (inMaxX - inMinX) * (outMaxX - outMinX) + outMinX;
    y = (y - inMinY) / (inMaxY - inMinY) * (outMaxY - outMinY) + outMinY;
    return {x, y};
}

function drawMap() {
    ctx.strokeStyle = '#c5b4a9';
    ctx.fillStyle = '#d9d0c9';
    for (const building of buildings) {
        ctx.beginPath();
        const pos0 = fixPosition({x: building[0].lon, y: building[0].lat});
        ctx.moveTo(pos0.x, pos0.y);
        for (let i = 1; i < building.length; i++) {
            const pos = fixPosition({x: building[i].lon, y: building[i].lat});
            ctx.lineTo(pos.x, pos.y);
        }
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
    ctx.fillStyle = 'red';
    for (const fireHydrant of fireHydrants) {
        const position = fixPosition({x: fireHydrant.lon, y: fireHydrant.lat});
        ctx.fillRect(position.x, position.y, 16, 16);
    }
}

var players = [];
players.push(new PlayerActor());

function drawPlayers() {
    for (const player of players) {
        const pos = player.pos();
        ctx.translate(pos.x, pos.y);
        ctx.fillStyle = '#00f';
        ctx.fillRect(32, 32, 32, 16);

        // reset current transformation matrix to the identity matrix
        ctx.setTransform(1, 0, 0, 1, 0, 0);
    }
}

function draw() {
    drawMap();
    drawPlayers();
}

function drawLoop() {
    draw();
}

/* === Game Loop === */
function step(timestamp) {
    const player = players[0];
    
    if (buildings.length) window.requestAnimationFrame(step);
}

function resizeCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}

window.addEventListener('resize', function (event) {
    resizeCanvas();
    draw();
});

resizeCanvas();

const map = createMapChunk();
