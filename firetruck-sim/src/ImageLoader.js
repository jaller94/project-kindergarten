export default class ImageLoader {
    constructor(callback) {
        this.callback = callback;
        this.loading = 0;
    }

    imgCounter() {
        this.loading--;
        if (this.loading === 0) {
            this.callback();
        }
    }

    loadImage(src) {
        const img = new Image();
        img.onload = this.imgCounter.bind(this);
        this.loading++;
        img.src = 'static/' + src;
        return img;
    }
}
