'use strict';

const fs = require('fs');
const readline = require('readline');
const util = require('util');

const uuid = require('node-uuid');


var lineReader = readline.createInterface({
	input: fs.createReadStream('./rules.txt')
});

var classes = {};

function validateSuperClasses(classes) {
	//TODO Check for circles
	Object.keys(classes).forEach( function(current, index, array) {
		index = current;
		current = classes[current];
		array = classes;
		if (current['super'] != null) {
			if (array[current['super']] == null) {
				console.error('Superclass %s for class %s does not exist!', current['super'], index);
			}
		}
	});
}

lineReader.on('line', function (line) {
	console.log('Line from file:', line);
	
	var regex = /^class (\w+)( extends ([\w]+))?$/
	var comment = /^#/
	var result = line.match(regex);
	if (result) {
		var name = result[1];
		var superclass = result[3];
		var jclass = {};
		if (superclass != null) {
			jclass['super'] = superclass;
		}
		classes[name] = jclass;
	} else if ((line == '') || (line.match(comment))) {
	} else {
		console.error('Error parsing line:', line);
	}
});

lineReader.on('close', function() {
	console.log(classes);
	validateSuperClasses(classes);
});
