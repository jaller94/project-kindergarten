'use strict';

class Counter {
	constructor(parentDiv) {
		this.food = null;
		this.elem = document.createElement('span');
		this.elem.classList.add('foodSlot');
		parentDiv.appendChild(this.elem);
	}

	setFood(food = null) {
		this.food = food;
		if (this.food === null) {
			while (this.elem.firstChild) {
				this.elem.removeChild();
			}
		}
	}
}

var gamepad = null;
var counter_perfect = 0;
var counter_ok = 0;
var counter_bad = 0;
var label_perfect = document.getElementById('label-perfect');
var label_ok = document.getElementById('label-ok');
var label_bad = document.getElementById('label-bad');
var button_was_pressed = false;
var button_down = false;
var lastTime = new Date().getTime();

var counter_list = new Array(5);
var counter_current_position = 0;

var html_counter = document.getElementById('counter');

for (let i = 0; i < 5; i++) {
	counter_list[i] = new Counter(html_counter);
}

var kitchen = {
	html_workspace: document.getElementById('workspace'),
	html_description: document.getElementById('meal_description'),
	html_instructions: document.getElementById('meal_instructions'),
	meal: null
}

function removeAllChildren(element) {
	while (element.firstChild) {
		element.removeChild(element.firstChild);
	}
}

function clearKitchen() {
	kitchen.meal = null;
	removeAllChildren( kitchen.html_workspace );
	removeAllChildren( kitchen.html_description );
	removeAllChildren( kitchen.html_instructions );
}

function setupFries() {
	// Our fries
	const fries = document.createElement('div');
	fries.classList.add('fries');
	// Our deep fryer to dip the fries into
	const deepfryer = document.createElement('div');
	deepfryer.classList.add('deepfryer');

	// Add the elements to the existing workplace
	kitchen.html_workspace.appendChild(fries);
	kitchen.html_workspace.appendChild(deepfryer);

	const faces = {
		fries: fries,
		deepfryer: deepfryer
	}
	return faces;
}

function actFries(deltaTime, controls) {
	if (controls instanceof Gamepad) {
		this.height = controls.axes[1];
	} else {
		this.height = button_down ? 1 : 0;
	}

	if (this.height > 0.5) {
		this.time += deltaTime;
	}

	const fries_progress = this.time / 2 / 1000;

	this.faces.fries.style.top = (this.height * 7 + 1) + 'em';
	const r = (255 - (fries_progress * 100)).toFixed(0);
	const g = (255 - (fries_progress * 200)).toFixed(0);
	const b = (25 - (fries_progress * 12)).toFixed(0);
	this.faces.fries.style.backgroundColor = 'rgb('+r+','+g+','+b+')';

	meal_progress.value = fries_progress;
}

function finishFries() {
	if (this.height < 0.3) {
		if (this.time / 1000 > 1.8 && this.time / 1000 < 2.3) {
			return 'perfect';
		} else {
			return 'bad';
		}
	}
	return false;
}

function createFries() {
	const fries = {
		time: 0,
		in_since: 0,
		height: 0,
		act: actFries,
		finish: finishFries
	}
	fries.faces = setupFries();
	return fries;
}

function buttonPressed(b) {
	if (typeof(b) == "object") {
		return b.pressed;
	}
	return b == 1.0;
}

function checkoutMeal() {
	const rating = kitchen.meal.finish();
	if (!rating) {
		return false;
	}

	switch(rating) {
		case 'perfect':
			counter_perfect++;
			break;
		case 'ok':
			counter_ok++;
			break;
		case 'bad':
			counter_bad++;
			break;
	}
	clearKitchen();

	label_perfect.innerText = counter_perfect.toString();
	label_bad.innerText = counter_bad.toString();
}

function checkinMeal() {
	if (kitchen.meal != null) {
		return false;
	}

console.log('k');
	kitchen.meal = createFries();
	return true;
}

function testButtons(gamepad) {
	if (buttonPressed(gamepad.buttons[5])) {
		if (!button_was_pressed) {
			button_was_pressed = true;
			checkoutMeal();
			checkinMeal(); //TODO Move this to another button
		}
	} else {
		button_was_pressed = false;
	}
}

function gameLoop() {
	var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
	if (!gamepads) {
		return;
	}

	let controls = {};
	if (gamepads[0]) {
		controls = gamepads[0];
	}

	const now = new Date().getTime();
	const deltaTime = now - lastTime;
	lastTime = now;

	if (kitchen.meal) {
		kitchen.meal.act(deltaTime, controls);
	}

	if (gamepads[0]) {
		testButtons(gamepads[0]);
	}

	requestAnimationFrame(gameLoop);
}

window.addEventListener("gamepadconnected", function(e) {
	console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
		e.gamepad.index, e.gamepad.id,
		e.gamepad.buttons.length, e.gamepad.axes.length);
		gamepad = e.gamepad;
});

window.addEventListener("gamepaddisconnected", function(e) {
	console.log("Gamepad disconnected from index %d: %s",
		e.gamepad.index, e.gamepad.id);
});

window.addEventListener("keydown", function(e) {
	if (e.keyCode === 40) {
		button_down = true;
	} else if (e.keyCode === 45) {
		checkoutMeal();
		checkinMeal();
	}
});

window.addEventListener("keyup", function(e) {
	if (e.keyCode === 40) {
		button_down = false;
	}
});

kitchen.meal = createFries();

requestAnimationFrame(gameLoop);
