# 2D Game
Simple clone of CS2D.

## Server
1. Navigate into the server folder.
2. `npm install`
3. `npm start`

## Client
1. Navigate into the client folder.
2. `npm install`
3. `npm dev`

A web server should have started up at http://localhost:1234/.
