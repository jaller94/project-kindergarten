'use strict';

const WebSocket = require('ws');

let lastProgress = 0;
let progress = 0;

const options = {
  port: 8080,
};

const wss = new WebSocket.Server(options);

// Broadcast to all.
wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  });
};

wss.on('connection', function connection(ws) {
  console.log(ws);
  console.log('New connection');
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    if (message === 'f') {
      progress += 1;
    }
  });
  ws.on('close', function (code, reason) {
    console.log('Connection closed');
  });

  ws.send('something');
});

setInterval(() => {
  if (lastProgress !== progress) {
    console.log('progress', progress);
    wss.broadcast(progress.toString());
    lastProgress = progress;
  }
}, 50);
