export const createMapChunk = () => {
  const mapchunk = [];
  for (let y = 0; y < 48; y++) {
    const row = [];
    for (let x = 0; x < 48; x++) {
      const items = [8,9,10,15,16,20,24,25,26,27];
      const randItem = items[Math.floor(Math.random()*items.length)];
      row.push(randItem);
    }
    mapchunk.push(row);
  }
  return mapchunk;
};

export class MapChunk {
  constructor(mapchunk) {
    this.mapchunk = mapchunk;
  }

  getHeight(x, y) {
    if (x < 0) return 0;
    if (y < 0) return 0;
    if (x >= this.mapchunk.length) return 0;
    if (y >= this.mapchunk[0].length) return 0;
    const tilemapHeightInfo = getTilemapInfo().height;
    const tile = this.getTile(x, y);
    const height = tilemapHeightInfo[tile];
    if (height === undefined) {
      throw new Error(`Unable to determine height for tile: ${tile}`);
    }
    return height;
  }

  getTile(x, y) {
    return this.mapchunk[x][y];
  }
}

export const getTilemapInfo = () => {
  return {
    height: {
      0: 0,
      1: 2,
      2: 2,
      3: 2,
      4: 2,
      5: 2,
      6: 2,
      7: 2,
      8: 0,
      9: 0,
      10: 0,
      11: 0,
      12: 0,
      13: 0,
      14: 0,
      15: 0,
      16: 0,
      17: 0,
      18: 0,
      19: 0,
      20: 2,
      21: 2,
      22: 0,
      23: 0,
      24: 0,
      25: 0,
      26: 0,
      27: 0,
      28: 0,
      29: 0,
      30: 0,
      31: 0,
      32: 0,
      33: 0,
    },
  };
}
