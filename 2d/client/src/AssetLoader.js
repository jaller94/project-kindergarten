export default class AssetLoader {
  constructor(callback) {
    this.callback = callback;
    this.loading = 0;
  }

  assetCounter() {
    this.loading--;
    if (this.loading === 0) {
      this.callback();
    }
  }

  loadImage(src) {
    const image = new Image();
    image.onload = this.assetCounter.bind(this);
    this.loading++;
    image.src = './' + src;
    return image;
  }

  loadAudio(src) {
    var audio = new Audio();
    audio.onload = this.assetCounter.bind(this);
    audio.src = './' + src;
    audio.preload = 'auto';
    return audio;
  }
}
