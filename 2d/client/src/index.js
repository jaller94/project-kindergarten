import PlayerActor from './PlayerActor';
import GamepadInput from './input/GamepadInput';
import KeyboardInput from './input/KeyboardInput';
import {createMapChunk, MapChunk} from './MapChunk';

function startGame() {
  console.info('All images loaded!');
  window.requestAnimationFrame(step);
}

import AssetLoader from './AssetLoader.js';

const loader = new AssetLoader(startGame);

const ct1 = loader.loadImage('gfx/player/ct1.png');
const deagle = loader.loadImage('gfx/weapons/deagle.png');
const tileset = loader.loadImage('gfx/tiles/embrador.png');
const shadowSet = loader.loadImage('gfx/shadowmap.png');

const deagleAudio = loader.loadAudio('sfx/weapons/deagle.ogg');
const clipOutAudio = loader.loadAudio('sfx/weapons/w_clipout.ogg');

const canvas = document.getElementById('main');
const ctx = canvas.getContext('2d');

var audioCtx = new AudioContext();

/* === Websocket Handling */
let socket = new WebSocket('ws://localhost:8080/', 'simplePaddleProtocol');

socket.onopen = function (event) {
  //TODO Initial data exchange
}

socket.onmessage = function (event) {
  try {
    console.log(event.data);
  } catch (error) {
    if (error) {
      console.error(error);
    } else {
      console.error('There was an unkown WebSocket error!');
    }
  }
}


/* === User Input Handling === */
window.addEventListener('beforeunload', function(){
  socket.close();
});

function drawTile(ctx, tileset, tile, xPix, yPix) {
  const columns = Math.floor(tileset.width / 32);
  const xTile = (tile % columns) * 32;
  const yTile = Math.floor(tile / columns) * 32;
  ctx.drawImage(tileset, xTile, yTile, 32, 32, xPix, yPix, 32, 32);
}

function drawMap() {
  for (let y = 0; y < map[0].length; y++) {
    for (let x = 0; x < map.length; x++) {
      drawTile(ctx, tileset, map[x][y], x*32, y*32);
    }
  }
}

function drawShadowMap() {
  for (let y = 0; y < map[0].length; y++) {
    for (let x = 0; x < map.length; x++) {
      const diffLeft = mapchunk.getHeight(x-1, y) - mapchunk.getHeight(x, y);
      const diffUpLeft = mapchunk.getHeight(x-1, y-1) - mapchunk.getHeight(x, y);
      const diffUp = mapchunk.getHeight(x, y-1) - mapchunk.getHeight(x, y);
      const a = {
        '022': 0,
        '011': 1,
        '002': 2,
        '001': 3,
        '020': 4,
        '010': 5,
        '220': 6,
        '110': 7,
        '200': 8,
        '100': 9,
        '2x2': 10,
        '1x2': 11,
        '2x1': 12,
        '1x1': 13,
        '210': 14,
        '120': 15,
        '012': 16,
        '021': 17,
      };
      let tile = a[`${diffLeft}${diffUpLeft}${diffUp}`];
      if (tile === undefined) {
        tile = a[`${diffLeft}x${diffUp}`];
      }
      if (tile !== undefined) {
        drawTile(ctx, shadowSet, tile, x*32, y*32);
      }
    }
  }
}

let players = [];
let shots = [];
players.push(new PlayerActor());
let xMouse = 20;
let yMouse = 20;

function drawPlayers() {
  for (const player of players) {
    const pos = player.pos();
    ctx.translate(pos.x, pos.y);
    ctx.rotate(player.ang());
    ctx.drawImage(ct1, 32, 0, 32, 32, -16, -16, 32, 32);
    ctx.drawImage(deagle, 0, 0, 32, 32, -16, -32, 32, 32);

    // reset current transformation matrix to the identity matrix
    ctx.setTransform(1, 0, 0, 1, 0, 0);
  }
}

function drawShots() {
  for (const shot of shots) {
    ctx.strokeStyle = '#FF8';
    ctx.beginPath();
    ctx.moveTo(shot.x, shot.y);
    ctx.lineTo(shot.targetX, shot.targetY);
    ctx.stroke();
  }
}

function draw() {
  drawMap();
  drawShadowMap();
  drawShots();
  drawPlayers();
}

function mapCollision(x, y, r, vector) {
  
}

/* === Game Loop */
var lastTimestamp = 0;
var lastShot = 0;
function step(timestamp) {
  draw();
  shots = [];
  const player = players[0];
  const delta = (timestamp - lastTimestamp) / 1000;
  lastTimestamp = timestamp;
  if (!input) {
    player.move(keyboardInput.movement(), delta);
    const playerPos = player.pos();
    const x = xMouse - playerPos.x;
    const y = yMouse - playerPos.y;
    const ang = Math.atan2(x, -y);
    player.setAngle(ang);
    if (shooting) {
      lastShot -= delta;
      if (lastShot <= 0) {
        lastShot = 1;
        const pos = players[0].pos();
        shoot(pos.x, pos.y, xMouse, yMouse);  
      }
    } else {
      lastShot = 0;
    }
  } else {
    player.move(input.movement(), delta);
    const target = input.target();
    const ang = Math.atan2(target.x, -target.y);
    player.setAngle(ang);
    input.act();
  }
  if (input2) {
    players[1].move(input2.movement(), delta);
    const target = input2.target();
    const ang = Math.atan2(target.x, -target.y);
    players[1].setAngle(ang);
    input2.act();
  }
  window.requestAnimationFrame(step);
}

function resizeCanvas() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}

function dragging(e) {
  xMouse = e.clientX;
  yMouse = e.clientY;
}

var shooting = false;
function mouseDown(e) {
  shooting = true;
}

function mouseUp(e) {
  shooting = false;
}

function shoot(x, y, targetX, targetY) {
  const audio = new Audio(deagleAudio.src);
  const source = audioCtx.createMediaElementSource(audio);
  const panNode = audioCtx.createStereoPanner();
  panNode.pan.value = (x - 600) / 600;
  console.log(panNode.pan.value);
  source.connect(panNode)
  panNode.connect(audioCtx.destination);
  audio.play();
  shots.push({
    x,
    y,
    targetX,
    targetY,
  });
}

function reload() {
  clipOutAudio.play();
}

canvas.addEventListener('mousemove', dragging);

canvas.addEventListener('mousedown', mouseDown);
canvas.addEventListener('mouseup', mouseUp);

window.addEventListener('resize', function (event) {
  resizeCanvas();
  draw();
});

const commandServer = {
  send: (cmd) => {
    if (cmd === 'increase') {
      socket.send('f');
    } else if (cmd === 'reload') {
      reload();
    }
  }
}

var keyboardInput = new KeyboardInput(document, commandServer);

var input;
var input2;

window.addEventListener('gamepadconnected', function(e) {
  if (!input) {
    input = new GamepadInput(e.gamepad, commandServer);
  } else {
    input2 = new GamepadInput(e.gamepad, commandServer);
    players.push(new PlayerActor());
  }
  console.info('New gamepad:', e.gamepad.index, e.gamepad.id);
});

resizeCanvas();

const map = createMapChunk();
const mapchunk = new MapChunk(map);
