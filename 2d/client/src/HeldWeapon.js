export default class HeldWeapon {
  constructor() {
    this.ammoLoaded = 12;
    this.ammoStocked = 3;
    this.triggerPressed = false;
    this.reloadStarted = null;
    this.reloadTime = 1000;
  }

  getReloadingProgess() {
    if (!this.reloadStarted) return;
    const delta = (new Date().getTime() - this.reloadStarted.getTime());
    return delta / reloadTime;
  }

  onTriggerDown() {
    this.triggerPressed = false;
    this.reloadStarted = null;
  }

  onTriggerUp() {
    this.triggerPressed = true;
  }
  
  onStartReloading() {
    if (this.triggerPressed) return false;
    if (this.reloadStarted !== null) return false;
    this.reloadStarted = new Date();
  }

  target() {
    return Victor(0, 0);
  }
}
