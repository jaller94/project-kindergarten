import Victor from 'victor';

function remapKeys(key) {
  if (key === 'Up') return 'ArrowUp';
  if (key === 'Right') return 'ArrowRight';
  if (key === 'Down') return 'ArrowDown';
  if (key === 'Left') return 'ArrowLeft';
  if (key === 'Space') return ' ';
  return key;
}

export default class KeyboardInput {
  constructor(node, commandServer) {
    this.keysPressed = {};

    node.addEventListener('keydown', (event) => {
      const key = remapKeys(event.key);
      const keyUpperCase = key.toUpperCase();
      if (key === ' ') {
        commandServer.send('increase');
      }
      this.keysPressed[keyUpperCase] = true;
      if (keyUpperCase === 'R') {
        commandServer.send('reload');
      }
    });

    node.addEventListener('keyup', (event) => {
      const key = remapKeys(event.key);
      this.keysPressed[key.toUpperCase()] = false;
    });
  }

  movement() {
    let x = 0;
    if (this.keysPressed['A'] && !this.keysPressed['D']) {
      x = -1;
    }
    if (this.keysPressed['D'] && !this.keysPressed['A']) {
      x = 1;
    }
    let y = 0;
    if (this.keysPressed['W'] && !this.keysPressed['S']) {
      y = -1;
    }
    if (this.keysPressed['S'] && !this.keysPressed['W']) {
      y = 1;
    }
    return Victor(x, y);
  }

  target() {
    return Victor(0, 0);
  }
}
