'use strict';

var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

console.log(canvas, ctx);

var camera = 0;
var actors = [];

function act() {
	controls();
	actors.forEach(function (actor) {
		actor.act();
	});
}

function controls() {
	if (keyStatus('keyup')) {
		actors.push(
			new LaserActor()
		);
		console.log('Laser fired!');
	}
	if (keyStatus('keydown')) {
	}
	if (keyStatus('keyleft')) {
	}
	if (keyStatus('keyright')) {
	}
	if (keyStatus('keyhandbreak')) {
	}
}

function draw() {
	ctx.resetTransform();

	//ctx.rect(0, 0, 400, 400);
	//ctx.clip();

	ctx.fillStyle = 'black';
	ctx.fillRect(0,0, canvas.clientWidth,canvas.clientHeight);

	//ctx.translate();

	ctx.scale(15,15);

	actors.forEach(function (actor) {
		ctx.save();
		const position = actor.getPosition();
		ctx.translate(position.x, position.y);
		ctx.rotate(position.rot);
		actor.draw(ctx);
		ctx.restore();
	});

	ctx.resetTransform();
	ctx.fillStyle = 'black';
	ctx.font='10px Courier';
	//ctx.fillText('Wheel: ' + car.wheels['back'].wheel.speed.toFixed(2) + 'm/s',0,370);
	//ctx.fillText('Speed: ' + car.speed.toFixed(2) + 'm/s',0,380);
	//ctx.fillText('Speed: ' + (car.speed * 3.6).toFixed(2) + 'km/h',0,390);

	requestAnimationFrame(draw);
}

var w = window;
requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

draw();
setInterval(function(){ act() }, 33);

bindStatus('keyup', 38);
bindStatus('keydown', 40);
bindStatus('keyleft', 37);
bindStatus('keyright', 39);
bindStatus('keyhandbreak', 32);

bindDown( function() {
	camera = 0;
}, 49);
bindDown( function() {
	camera = 1;
}, 50);
