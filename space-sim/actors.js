class Vector2D {
	constructor(x, y, rot) {
		this.x = x;
		this.y = y;
		this.rot = rot % Math.Pi;
	}

	add(vector) {
		return new Vector2D(
			this.x + vector.x,
			this.y + vector.y,
			this.rot + vector.rot,
		)
	}
}

class Actor {
	constructor() {
		this.position = new Vector2D(0, 0, 0);
		this.velocity = new Vector2D(0, 0, 0);
	}

	act() {
		throw new Error('Not implemented');
	}

	draw(ctx) {
		throw new Error('Not implemented');
	}

	move(vector) {
		this.position = this.position.add(vector);
	}

	moveForward(vector) {
		//TODO Apply correct angle
		this.position = this.position.add(vector);
	}

	getPosition() {
		return this.position;
	}

	getVelocity() {
		return this.velocity;
	}

	setPosition(position) {
		this.position = position;
	}

	setVelocity(velocity) {
		this.position = position;
	}
}

class DotActor extends Actor {

}

class LaserActor extends Actor {
	act() {
		this.moveForward(new Vector2D(1,0,0));
	}

	draw(ctx) {
		ctx.fillStyle = 'lightblue';
		ctx.fillRect(-0.5,-0.5, 1,1);
	}
}
