# Quantum Text Parser

*This project is not finished! Don't even try to use it.*

A parser for "quantum authoring" for writing novels with occasional splitting and alternate words. This allows to write generative stories where each copy is unique.

The syntax is based on [Quantum Text by Aaron A. Reed](https://medium.com/@aareed/a-minimal-syntax-for-quantum-text-ac5b34308593).

## Use it
1. You need to have [NodeJS](https://nodejs.org/) and NPM installed. They allow to run the JavaScript code.
2. Download this respository to your computer and extract it to some folder.
3. Inside the folder run `npm install` to install dependencies of the code.
4. Run the tests with `npm run test` to verify everything is working as expected.
5. Have the program render a random copy of your quantum text by running: `node src/index.js "My Story.quant"`

### Seeded copies
Want a specific copy that stays the same each time you generate it? You may specify a seed to control the randomness.

```bash
node src/index.js "My Story.quant"` 53023
```

The variables will get the same values as long as:
* You don't change the name of variables.
* You don't add or remove alternatives of a DEFINE.

Adding or removing DEFINE statements won't have an effect on the others.
