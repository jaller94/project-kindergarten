const fsPromise = require('fs/promises');

const {
    parseDefines,
    pickAlternatives,
} = require('./helpers.js');

const defineRegEx = /\[DEFINE (\w+)\]/g;

async function main(args) {
    const filePath = args[0];
    const content = await fsPromise.readFile(filePath, 'utf8');
    const vars = parseDefines(content);
    console.log(vars);
    let result = content.replace(defineRegEx, '');
    result = pickAlternatives(result, vars);
    console.log(result);
}

main(process.argv.slice(2)).catch(console.error);
