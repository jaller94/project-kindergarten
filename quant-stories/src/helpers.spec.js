const {
    parseDefines,
    pickAlternatives,
} = require('./helpers.js');

describe('parseDefines', () => {
    test('returns no variables in a text without DEFINE', () => {
        expect(parseDefines('This is an example text.')).toStrictEqual({});
    });
    test('detects a variable in the same line as text', () => {
        const actual = parseDefines('[DEFINE var]This is an example text.');
        expect(actual).toHaveProperty('var');
    });
    test('detects a variable in a separate line', () => {
        const actual = parseDefines('[DEFINE var]\nThis is an example text.');
        expect(actual).toHaveProperty('var');
    });
    test('detects two variables in the same line', () => {
        const actual = parseDefines('[DEFINE var][DEFINE test]');
        expect(actual).toHaveProperty('var');
        expect(actual).toHaveProperty('test');
    });
    test('detects two variables in separate lines', () => {
        const actual = parseDefines('[DEFINE var]\n[DEFINE test]');
        expect(actual).toHaveProperty('var');
        expect(actual).toHaveProperty('test');
    });
});

describe('pickAlternatives', () => {
    test('returns the one option, if there is only one', () => {
        expect(pickAlternatives('This is an [example] text.')).toStrictEqual('This is an example text.');
    });
    test('returns the one option, if there are two', () => {
        expect(pickAlternatives('This is an [example|example] text.')).toStrictEqual('This is an example text.');
    });
    test('returns the one option, if there are three', () => {
        expect(pickAlternatives('This is an [example|example|example] text.')).toStrictEqual('This is an example text.');
    });
});
