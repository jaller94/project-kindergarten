const defineRegEx = /\[DEFINE (\w+)\]/g;
const alternativesRegEx = /\[([\w|]+)\]/gsm;

function parseDefines(content) {
    const vars = {};
    const matches = content.matchAll(defineRegEx);
    for (const match of matches) {
        const groups = match[1].split('|');
        for (const group of groups) {
            vars[group] = Math.random() > .5;
        }
    }
    return vars;
}

function pickAlternatives(content, vars) {
    const matches = content.matchAll(alternativesRegEx);
    for (const match of matches) {
        const groups = match[1].split('|');
        const group = groups[Math.floor(Math.random()*groups.length)];
        content = content.slice(0, match.index) + group + content.slice(match.index + match[0].length);
    }
    return content;
}

module.exports = {
    parseDefines,
    pickAlternatives,
};
