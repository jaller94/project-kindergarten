import React, { Component } from 'react';
import Clock from './Clock';
import TaskSelectorList from './taskselector/TaskSelectorList';
import Kitchen from './Kitchen';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {time: new Date()};
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({time: new Date()});
  }

  createKitchens(title) {
    return (
      <div>
        <Kitchen />
      </div>
    );
  }

  render() {
    return (
      <div className="App">
        {
          //<Clock time={this.state.time}/>
        }
        <TaskSelectorList length="5" />
      </div>
    );
  }
}

export default App;
