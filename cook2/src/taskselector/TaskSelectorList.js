import React, { Component } from 'react';
import TaskSelectorItem from './TaskSelectorItem';
import './TaskSelectorList.css';

class TaskSelectorList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slots: this.createSlots(this.props.length)
    };

    window.addEventListener("keydown",(e) => {
    	if (e.keyCode === 39) {
    		const slots = this.state.slots;
        slots[0].task =  {name: 'Fries'};
        this.setState({
          slots: slots
        });
    	}

    });
  }

  createSlots(length) {
    const slots = [];
    for (var i = 0; i < length; i++) {
      slots.push({
        number: i + 1,
        food: null
      });
    }
    return slots;
  }

  render() {
    const listOfSlots = this.state.slots.map((slot) => {
      return (
        <TaskSelectorItem key={slot.number} {...slot} />
      );
    })
    return (
      <ol className="TaskSelectorList">
        {listOfSlots}
      </ol>
    );
  }
}

export default TaskSelectorList;
