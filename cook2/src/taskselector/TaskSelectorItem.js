import React, { Component } from 'react';

class TaskSelectorItem extends Component {
  render() {
    return (
      <li className="TaskSelectorItem">
        <div class="ItemNumber">{this.props.number}</div>
        { this.props.task && (
          <div>{this.props.task.name}</div>
        )}
      </li>
    );
  }
}

export default TaskSelectorItem;
