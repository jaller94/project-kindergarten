"use strict";

const label_total = document.getElementById('counter');
var speed = kmh2ms(110);
var total = 0;
var trip = 0;
var trip_length = 4 * 1000;
var lasttick = new Date().getTime();

/* Global functions */
function kmh2ms(kmh) {
	return kmh / 3.6;
}

function ms2kmh(ms) {
	return ms * 3.6;
}

Array.prototype.getRandom = function() {
	return this[Math.floor(Math.random()*this.length)];
}

/* Interface & Flow */
function updateMeter() {
	const progress = document.getElementById('progress');
	progress.value = trip / trip_length;
}

function act() {
	const time = new Date().getTime();
	const delta = (time - lasttick) / 1000;
	lasttick = time;

	if (trip < trip_length) {
		drive(delta);
		if (!(trip < trip_length)) {
			notify('We\'ve arrived at ' + generateTownName() + '!');
		}
	}

	updateMeter();

	label_total.textContent = (total/1000).toFixed(2) + ' km';
}

function startTrip() {
	trip = 0;
	trip_length = (60 + Math.random() * 50) * 1000;
}

/* World & Trains */
function generateTownName() {
	const names = "James,Mary,Victoria,Chris,John,Bob,Pete,Peters,William".split(',');
	const endings = "ville,ville,town,polis,burg,ford,ton, City".split(',');
	return names.getRandom() + endings.getRandom();
}

function drive(delta) {
	let newmeters = speed * delta * 60;
	newmeters = Math.min(newmeters, trip_length - trip);

	trip += newmeters;
	total += newmeters;
}

/* Notifications */

function notify(message) {
	// Does the browser support Notifications?
	if (!("Notification" in window)) {
		// No, so notifications are disabled
		return;
	}

	// Did the user grant us to send notifications?
	if (Notification.permission !== "granted") {
		// No, so we won't bother the user
		return;
	}

	return new Notification(message);
}

function setupNotifications() {
	// Let's check if the browser supports notifications
	if (!('Notification' in window)) {
		alert('This browser does not support desktop notifications!');
	}

	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === 'granted') {
		// If it's okay let's create a notification
		var notification = new Notification('Notifications are now enabled.');
	}

	// Otherwise, we need to ask the user for permission
	else if (Notification.permission !== 'denied') {
		Notification.requestPermission().then(function(permission) {
			// If the user accepts, let's create a notification
			if (permission === "granted") {
				var notification = new Notification("From now on you will get notifications!");
			}
		});
	}

	// At last, if the user has denied notifications, and you 
	// want to be respectful there is no need to bother them any more.
}

setInterval(act, 1000);
