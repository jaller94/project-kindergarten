'use strict';

const textArea = document.getElementById('text_story');
const buttons_actions = document.getElementById('buttons_actions');

let actionButtonsCallback = null;


function setActionButtons(actionButtons, callback) {
	// Clear old buttons
	buttons_actions.textContent = '';
	// Add new buttons
	for (const actionButton of actionButtons) {
		const button = document.createElement('button');
		button.type = 'button';
		button.textContent = actionButton.label;
		// Add EventListener and make it pass the event and the action
		if (actionButton.callback == undefined) {
			button.addEventListener('click', () => {
				console.log('standard callback');
				callback.call(this, actionButton.action);
			});
		} else {
			button.addEventListener('click', () => {
				console.log('custom callback');
				actionButton.callback.call(this, actionButton.action);
			});
		}
		buttons_actions.appendChild(button);
	}
}

function setStoryText(text) {
	textArea.textContent = text;
}

const occupations = {
	'Chemist': {
		'chemical analysis': 3,
		'computers': 1,
		'strength': -3,
		'tools': 1
	},
	'Computer Scientist': {
		'chemical analysis': 1,
		'computers': 3,
		'strength': -1,
		'tools': 1
	},
	'Sportsman': {
		'chemical analysis': -2,
		'computers': -2,
		'strength': 3,
		'tools': -1
	},
	'Theatre Technician': {
		'chemical analysis': -1,
		'computers': 1,
		'strength': 1,
		'tools': 3
	}
};

const gender = {
	'Man': {
		'strength': 1,
		'perception': -1
	},
	'Woman': {
		'strength': -1,
		'perception': 1
	},
	'Other': {
		'strength': 0,
		'perception': 0
	}
};

class Human {
	constructor() {
		this.attributes = {};
	}

	getAttributes() {
		//TODO This is an object, not an array
		return this.attributes.slice(0);
	}

	applyAttributesSet(attributes) {
		for(const attribute in attributes) {
			if (this.attributes.hasOwnProperty(attribute)) {
				// Increase/Decrease the value
				this.attributes[attribute] += attributes[attribute];
			} else {
				// Set this new value
				this.attributes[attribute] = attributes[attribute];
			}
		}
	}
}

function createPictures(human) {
  const allpictures = [];
  let picture = {
    type: 'picture',
  }
  // Family picture
  allpictures[0] = Object.assign({}, picture, {
    subtype: 'family',
    description: `It's a photo of your family back on Earth. Seeing their faces and the mountain view of your childhood warms your heart.`,
  });
  // Childhood friend
  allpictures[1] = Object.assign({}, picture, {
    subtype: 'childhood friend',
    description: `It's a photo of your childhood friend Pete. When he became a researcher you kind of lost contact.`,
  });
  // Ex
  allpictures[2] = Object.assign({}, picture, {
    subtype: 'ex',
    description: `It's a photo of your your ex Olivia. It's clear you were not each other's type but you still miss her.`,
  });
  return allpictures;
}

function createPodInventory() {
  const items = [];
  return items;
}

function characterSelection(choices, callback) {
	console.log('new Charater Choice');
	const buttons = [];
	for (const key in choices) {
		const button = {
			'label': key,
			'action': key
		};
		buttons.push(button);
	}
	setActionButtons(buttons, callback);
}

let storylayers = [];
let human = new Human();

function story() {
	setStoryText('Please select the former occupation of your character!');
	characterSelection(occupations, story2);
}

function story2(occupation) {
	human.applyAttributesSet(occupation);

	setStoryText('Please select a gender for your character!');
	characterSelection(gender, story3);
}

function story3(gender) {
	human.applyAttributesSet(gender);

	const buttons = [{'label': 'Leave the pod'}];
	setStoryText(`You are stranded on a foreign planet. From the inside of your small escape pod you look through the front window.
	The planet's surface is very flat and covered in ice. A snow storm is coming up, however, you should be fine if you stay within 100 meters of a shelter. You do not see any life or vegetation.`);

	setActionButtons(buttons, story4);
}

function story4() {
	let text = 'You step out of the escape pod.';
	let hurt = 'Your neck is hurt from the crash. It will prevent you from spoting things behind you.';
	const buttons = [{'label': 'Look around'}];

	setStoryText(text + '\n' + hurt);
	setActionButtons(buttons, story5);
}

function story5() {
	let look = `You look around but you don't see anything familiar.`;
	let friends = 'You were traveling on your own and now you might be stuck on this planet.';
	const buttons = [{'label': 'Inspect the ground'}];

	setStoryText(look + '\n' + friends);
	setActionButtons(buttons, story6);
}

function story6() {
	story_near_pod();
}

function story_near_pod() {
	const buttons = [
		{
			'label': `Open pod's inventory`,
			'callback': story_pod_inventory
		},
		{
			'label': `What to do?`,
			'callback': story_mission_near_pod
		},
		{'label': `Go exploring`}
	];

	setStoryText(`The planet has an atmosphere and your wristband says it's breathable. However, it is way too cold to walk around without a spacesuit.`);
	setActionButtons(buttons, story_leave_pod);
}

function story_mission_near_pod() {
	const buttons = [
		{
			'label': `Ok`
		}
	];

	setStoryText(`Your mission is TODO.`);
	setActionButtons(buttons, story_near_pod);
}

function story_leave_pod() {
	const buttons = [
		{
			'label': `Approach pod`,
			'callback': story_near_pod,
		},
		{
			'label': `What to do?`,
			'callback': story_todo
		}
	];

	setStoryText(`You walk away from your pod. The pod is of little interest of your progress but for now it's the only shelter you know of. Don't lose track of where it is, for now!`);
	setActionButtons(buttons, story_pod_inventory);
}

function story_item_description(itemId) {
	const buttons = [
		{'label': `Ok`}
	];

	let description = 'No description!';

	console.log('itemId', itemId);

	if (itemId === 'flashlight') {
		description = 'A flashlight that casts a circular beam of light. It increases your vision at night.';
	} else if (itemId === 'ranger axe') {
		description = 'A ranger axe that is good for cutting solid materials, like wood and ice.';
	}

	setStoryText(description);
	setActionButtons(buttons, story_pod_inventory);
}

function story_pod_inventory() {
	const buttons = [
		{
			'label': `Flashlight`,
			'action': 'flashlight',
			'callback': story_item_description
		},
		{
			'label': `Ranger Axe`,
			'action': 'ranger axe',
			'callback': story_item_description
		},
		{
			'label': `Exit pod's inventory`,
			'action': 'exit',
			'callback': story_near_pod,
		},
	];

	setStoryText(`You open the emergency pod's inventory. You may carry two items with a maximum weight of 10 kilograms.`);
	setActionButtons(buttons);
}

function story_todo() {
	alert('story_todo: This is a dead end.');
}

story();
