let state = 'login';

class Account {
	constructor(name) {
		this.name = name;
		this.vars = {};
		this.history = [];
	}

	reset() {
		this.vars = {};
	}
}

class Player {

}

function addToHistory(received, sent) {
	const entry = {};
	entry.time = Date.now();
	if (received) {
		entry.received = received;
	}
	if (sent) {
		entry.sent = sent;
	}
	this.history.push(entry);
}

function getHistory() {
	return this.history;
}

let accounts = [];

accounts.push(new Account('Victoria'));
accounts.push(new Account('Christian'));
accounts.push(new Account('Guest 1'));
accounts.push(new Account('Guest 2'));
