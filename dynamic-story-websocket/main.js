const ws = require("nodejs-websocket");


let connections = [];

function sendAll(data) {
	for (const conn of connections) {
		conn.send(data);
	}
}

const server = ws.createServer(function (conn) {
	console.log("New connection");

	connections.push(conn);
	const scene = {
		'type': 'scene',
		'text': 'Hi, who are you?',
		'buttons': [
			{
				'label': 'Victoria',
				'action': 'victoria'
			},
			{
				'label': 'Christian',
				'action': 'christian'
			},
			{
				'label': 'Guest 1',
				'action': 'guest1'
			},
			{
				'label': 'Guest 2',
				'action': 'guest2'
			}
		]
	};
	conn.send(JSON.stringify(scene));

	conn.on("text", function (str) {
		console.log("Received "+str);

		let data;
		try {
			data = JSON.parse(str);
		} catch (err) {
			console.log('No JSON: ' + str);
			return;
		}

		const text = 'Thanks for visiting.. sadly, there is no content here yet. Come by later. ;)';
		const scene = {
			'type': 'scene',
			'text': text,
			'buttons': [
				{
					'label': 'Ok',
					'action': 'ok'
				}
			]
		};
		sendAll(JSON.stringify(scene));
	});
	conn.on("close", function (code, reason) {
		connections.splice(connections.indexOf(conn),1);
		console.log("Connection closed");
	});
	conn.on("error", function (code, reason) {
		console.log('error');
	});
}).listen(8001);

const http = require('http');
const fs = require('fs');
const path = require('path');

http.createServer(function (request, response) {
	console.log('request starting...');

	let filePath = '.' + request.url;
	if (filePath === './')
		filePath = './index.html';

	let extname = path.extname(filePath);
	let contentType = 'text/html';
	switch (extname) {
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.css':
			contentType = 'text/css';
			break;
		case '.json':
			contentType = 'application/json';
			break;
		case '.png':
			contentType = 'image/png';
			break;
		case '.jpg':
			contentType = 'image/jpg';
			break;
		case '.wav':
			contentType = 'audio/wav';
			break;
	}

	fs.readFile(filePath, function(error, content) {
		if (error) {
			if(error.code === 'ENOENT'){
				fs.readFile('./404.html', function(error, content) {
					response.writeHead(200, { 'Content-Type': contentType });
					response.end(content, 'utf-8');
				});
			}
			else {
				response.writeHead(500);
				response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
				response.end();
			}
		}
		else {
			response.writeHead(200, { 'Content-Type': contentType });
			response.end(content, 'utf-8');
		}
	});

}).listen(8000);
