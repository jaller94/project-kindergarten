'use strict';

const div_textarea = document.getElementById('text');
const div_buttons = document.getElementById('buttons');

function clearButtons() {
	div_buttons.textContent = '';
}

function sceneText(text) {
	div_textarea.textContent = text;
}

let socket = new WebSocket("ws://46.81.140.63:8001");

socket.onmessage = function (event) {
	let data = {};
	try {
		data = JSON.parse(event.data);
	} catch (err) {
		console.log('Can\'t parse: ' + event.data);
	}

	if (data && data.type === 'scene') {
		sceneText(data.text);

		if (data.buttons) {
			clearButtons();
			for (const button of data.buttons) {
				const div_button = document.createElement('button');
				div_button.textContent = button.label;
				div_button.addEventListener('click', (event) => {
					socket.send(JSON.stringify({'action': button.action}));
				});
				div_buttons.appendChild(div_button);
			}
		}
	}
};
