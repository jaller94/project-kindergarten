const Victor = require('victor');

let empty = () => {return null;};

class Objects {
	constructor() {
		this.name = empty;
		this.description = empty;
		this.weight = empty;
		this.planet = null;
	}

	putOnPlanet(planet) {
		this.planet = null;
	}
}

class Place {
	constructor() {
		this.name = empty;
		this.capacity = 0; // how many people can stay here?
		this.description = empty;
		this.objects = [];
		this.position = empty;
		this.exits = [];
	}

}

class Planet {
	constructor() {
		this.minx = 0;
		this.miny = 0;
		this.maxx = 1000*1000;
		this.maxy = 1000*1000;
		this.gravity = (x, y) => {return 9;};
		this.height = (x, y) => {return 0;};
		this.wind = (x, y) => {return new Victor(0, 0);};
		this.objects = [];
		this.places = [];
	}

	addObject(object) {
		this.objects.add(object);
	}

	addPlace(place) {
		this.place.add(place);
	}

	/**
	 * Lists nearby objects sorted by shortest distance from the position.
	 * @param {Victor} position
	 * @param {number} radius - the limiting radius in which to look for in meters
	 * @return {Object[]} a list of nearby Objects
	 */
	getNearbyPlaces(position, radius) {

	}
}

class CrashingSpacecraft {
	constructor() {
		this.splintered = 0.2;
		this.exploded = 0.2;
		this.burned = 0.4;
	}

	crash(planet, position, velocity) {
		const storage = new Place();
		storage.name = () => {return 'Spacecraft Storage Unit';};
		storage.capacity = 8;
		storage.description = () => {return 'It\'s the storage unit of a crashed spacecraft. This is were space travelers store their food and other goods.';};
		storage.position = position.clone();
		planet.addPlace(storage);
	}
}