import React, { Component } from 'react';
import './Picture.css';

var palette = [
    [0,0,0],
    [255,255,255],
    [255,0,0],
    [0,255,0],
    [0,0,255],
    [127,0,0],
    [0,127,0],
    [0,0,127],
    [127,127,0],
    [127,0,127],
    [0,127,127],
    [255,255,0],
    [255,0,255],
    [0,255,255],
];

// use Euclidian distance to find closest color
// send in the rgb of the pixel to be substituted
function mapColorToPalette(palette, red, green, blue) {
    var color, diffR, diffG, diffB, diffDistance, mappedColor;
    var distance = 100000;
    for (var i = 0; i < palette.length; i++) {
        color = palette[i];
        diffR = (color[0] - red);
        diffG = (color[1] - green);
        diffB = (color[2] - blue);
        diffDistance = diffR*diffR + diffG*diffG + diffB*diffB;
        if (diffDistance < distance) {
            distance = diffDistance;
            mappedColor = i;
        }
    }
    return mappedColor;
}

class Picture extends Component {
    constructor(props) {
        super(props);
        const actual = [];
        for (var i = 0; i < props.width * props.height; i++) {
            actual[i] = null;
        }
        this.state = {actual: []};
    }

    handleImageLoad = (event) => {
        const image = this.refs.source;
        const ctx = this.refs.scaled.getContext('2d');
        console.log('drawing');
        ctx.drawImage(image, 0, 0, this.props.width, this.props.height);
        this.reduceImage();
        this.drawAll();
    }

    handleMouseDown = (event) => {
        event.preventDefault();
        event.stopPropagation();

        this.mouseDown = true;
        this.handleMouseOver(event);
    };

    handleMouseUp = (event) => {
        this.mouseDown = false;
    };

    handleMouseOver = (event) => {
        if (this.mouseDown) {
            this.paint(event.clientX, event.clientY, 0, true);
        }
    };

    paint = (x, y, color, inPixels = false) => {
        if (inPixels) {
            return this.paint(Math.floor(x/16), Math.floor(y/16), color);
        }
        const index = x + (y * this.props.width);
        if (this.state.actual[index] === color) return false;

        // Update state
        const actual = this.state.actual.slice(0);
        actual[index] = color;

        this.setState({actual});
        return true;
    };

    reduceImage = () => {
        var imageData = this.refs.scaled.getContext('2d').getImageData(0, 0, this.props.width, this.props.height);
        const pix = imageData.data;
        for (var i = 0; i < imageData.width * imageData.height; i++) {
            const color = mapColorToPalette(palette, pix[i*4], pix[i*4+1], pix[i*4+2]);
            pix[i*4] = palette[color][0]; // Red
            pix[i*4+1] = palette[color][1]; // Green
            pix[i*4+2] = palette[color][2]; // Blue
        };
        const ctx = this.refs.reduced.getContext('2d');
        ctx.clearRect(0, 0, this.refs.reduced.width, this.refs.reduced.height);
        ctx.putImageData(imageData, 0, 0);
    };

    drawAll = () => {
        const canvas = document.getElementById('canvas');
        const ctx = this.refs.canvas.getContext('2d');
        for (var i = 0; i < this.props.width; i++) {
            ctx.fillStyle = '#8ED6FF';
            ctx.fillRect(i*16, 0, 16, 16);
        }
    };

    render() {
        return (
            <div className="Picture">
                <img
                    ref="source"
                    className="source"
                    src={this.props.src}
                    alt="Source"
                    onLoad={this.handleImageLoad}
                />
                <canvas
                    ref="scaled"
                    width={this.props.width}
                    height={this.props.height}
                />
                <canvas
                    ref="reduced"
                    width={this.props.width}
                    height={this.props.height}
                />
                <canvas
                    ref="canvas"
                    onMouseDown={this.handleMouseDown}
                    onMouseUp={this.handleMouseUp}
                    onMouseOver={this.handleMouseOver}
                    onMouseLeave={this.handleMouseUp}
                    width={500}
                    height={500}
                />
            </div>
        );
    }
}

export default Picture;
