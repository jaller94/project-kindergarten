import React, { Component } from 'react';
import Picture from './Picture.js';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Picture src="image.jpg" width="60" height="40"/>
      </div>
    );
  }
}

export default App;
