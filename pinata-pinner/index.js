require('dotenv').config();
const axios = require('axios');

const userPinList = (pinataApiKey, pinataSecretApiKey) => {
  const url = `https://api.pinata.cloud/data/pinList`;
  return axios
    .get(url, {
      headers: {
        'pinata_api_key': pinataApiKey,
        'pinata_secret_api_key': pinataSecretApiKey
      }
    })
    .then(function (response) {
      console.log(JSON.stringify(response.data, null, 2));
    })
    .catch(function (error) {
      console.error(error);
    });
};

userPinList(process.env.PINATA_API_KEY, process.env.PINATA_API_SECRET)
  .catch(console.error);
