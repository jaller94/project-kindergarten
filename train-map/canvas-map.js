"use strict";

const canvas = document.getElementById("canvas-map");
const ctx = canvas.getContext("2d");

function drawTracks(map, tracks) {
	const towns = map.towns;

	ctx.strokeStyle = "green";
	ctx.beginPath();
	for (const track of tracks) {
		const x1 = towns[track.origin].x * 140;
		const y1 = towns[track.origin].y * 140;
		const x2 = towns[track.destination].x * 140;
		const y2 = towns[track.destination].y * 140;

		ctx.moveTo(x1,y1);
		ctx.lineTo(x2,y2);
	}
	ctx.stroke();
}

function drawTowns(map, towns) {
	for (const town of towns) {
		const x = town.x * 140;
		const y = town.y * 140;

		ctx.fillStyle = "red";
		ctx.fillRect(x-3, y-3, 6, 6);
	}
}

function draw() {
	const canvas = ctx.canvas;

	ctx.fillStyle = "white";
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	if (window.map) {
		const towns = window.map.towns;
		const tracks = window.map.tracks;

		drawTracks(map, tracks);
		drawTowns(map, towns);
	} else {
		ctx.fillStyle = "green";
		ctx.fillText('Map not loaded!', 10, 50);	
	}

	//requestAnimationFrame(draw);
}

requestAnimationFrame(draw);
