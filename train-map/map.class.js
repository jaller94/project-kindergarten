"use strict";

class Map {
	constructor() {
		this.towns = [];
		this.tracks = [];
	}

	static unserialize(data) {
		const map = new Map();

		for (towndata in data.towns) {
			const town = Town.fromJSON(towndata);
			map.addTown(town);
		}

		console.log('Map from JSON: ' + map);
		return map;
	}

	serialize() {
		const data = {
			tracks: this.tracks
		};

		data.towns = [];
		for (const town of this.towns) {
			data.towns.push(town.serialize());
		}

		return data;
	}

	addTrack(town1, town2) {
		const town1id = this.towns.indexOf(town1);
		const town2id = this.towns.indexOf(town2);

		const track = {
			origin: town1id,
			destination: town2id
		} 
		this.tracks.push(track);
	}

	addTown(town) {
		this.towns.push(town);
	}

	getTowns() {
		return map.towns;
	}
}


class Location {
	constructor(map, x, y) {
		this.map = map;
		this.x = x;
		this.y = y;
	}

	distanceTo(x, y) {
		const dx = x-this.x;
		const dy = y-this.y;
		return Math.sqrt(dx*dx + dy*dy);
	}
}


class Town extends Location {
	constructor(map, x, y) {
		super(map, x, y);
		this.name = "Unnamed town";
	}

	static unserialize(data) {
		const town = new Town(data.map, data.x, data.y);
		town.setName(data.name);
		console.log('Town from JSON: ' + town);
		return town;
	}

	serialize() {
		const data = {
			name: this.name,
			x: this.x,
			y: this.y
		};
		return data;
	}

	distanceTo(town, y) {
		let x = 0;
		if (y) {
			x = town;
		} else {
			x = town.x;
			y = town.y;
		}

		return super.distanceTo(x, y);
	}

	findNearestTowns(amount) {
		const result = [];

		const items = [];

		for (const town of this.map.getTowns()) {
			if (town != this) {
				const item = {
					town: town,
					distance: this.distanceTo(town)
				};
				items.push(item);
			}
		}

		console.log(items);

		items.sort(function(a, b) {
			if (a.distance < b.distance) {
				return -1;
			}
			if (a.distance > b.distance) {
				return 1;
			}

			// names must be equal
			return 0;
		});

		for (const item of items.slice(0,2)) {
			result.push(item.town);
		}

		return result;
	}

	setName(name) {
		this.name = name;
	}
}
