"use strict";

function randomItemFromArray(array) {
	return array[Math.floor(Math.random()*array.length)];
}

function generateTownName() {
	const names = "James,Mary,Victoria,Chris,John,Bob,Pete,Peters,William".split(',');
	const endings = "ville,ville,town,city,burg,ford,ton".split(',');
	return randomItemFromArray(names) + randomItemFromArray(endings);
}

function generateMap() {
	const map = new Map();

	for (var i = 0; i < 10; i++) {
		const town = new Town(map, Math.random(), Math.random());
		town.setName( generateTownName() );
		map.addTown(town);
	}

	return map;
}

function defineConnections(map) {
	for (const town of map.towns) {
		const plannedDestinations = town.findNearestTowns(2);
		for (const plannedDestination of plannedDestinations) {
			map.addTrack(town, plannedDestination);
			console.log('Added Track');
		}
	}
}

function newGame() {
	window.map = generateMap();
	defineConnections(window.map);
	console.log('Generated new map: ');
	console.log(map);
}

function loadGame() {
	if (window.localStorage) {
		const mapdata = JSON.parse(window.localStorage.getItem('map'));
		window.map = Map.unserialize(mapdata);
	}
}

function saveGame() {
	window.localStorage.setItem('map', JSON.stringify(window.map.serialize()));
}

function deleteSave() {
	window.localStorage.removeItem('map');
}

const btnNewGame = document.getElementById('btn-newgame');
const btnSaveGame = document.getElementById('btn-savegame');
const btnDeleteSave = document.getElementById('btn-deletesave');

btnNewGame.addEventListener('click', (event) => {
	newGame();
});
btnSaveGame.addEventListener('click', (event) => {
	saveGame();
});
btnDeleteSave.addEventListener('click', (event) => {
	deleteSave();
});

function notifyMe() {
	// Let's check if the browser supports notifications
	if (!("Notification" in window)) {
		alert("This browser does not support desktop notification");
	}

	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === "granted") {
		// If it's okay let's create a notification
		var notification = new Notification("Hi there!");
	}

	// Otherwise, we need to ask the user for permission
	else if (Notification.permission !== 'denied') {
		Notification.requestPermission(function (permission) {
			// If the user accepts, let's create a notification
			if (permission === "granted") {
				var notification = new Notification("Hi there!");
			}
		});
	}

	// At last, if the user has denied notifications, and you 
	// want to be respectful there is no need to bother them any more.
}

try {
	loadGame();
	saveGame();
} catch (exception) {
	console.log('Konnte ich nicht laden!');
	newGame();
}

defineConnections(window.map);

console.log( window.map.towns[0].distanceTo(window.map.towns[1]) );

//notifyMe();
