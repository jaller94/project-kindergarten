# Universe Generator
Generates a bunch of random names for distant, undiscovered worlds and puts them in a tree hierarchy.

## Get it running
```
npm install
npm start
```
