'use strict';

const util = require('util');
const uuid = require('node-uuid');

function removeReferences(obj) {
	var copy = Object.assign({}, obj);
	console.log('copy starts here');
	console.log(copy);

	Object.keys(copy).forEach( function(current, index, arr) {
		console.log('== key %s starts here ==', index);
		console.log(current);
		if (current !== null && typeof current === 'object') {
			current = {'reference': current.getID()};
		} else if (current !== null && typeof current === 'array') {
			current = removeReferences(current);
		}
		console.log('Removed reference in %s', index);
	});

	return copy;
}

class Thing {
	constructor(type) {
		this.uuid = uuid.v1();
		this.type = type;
		this.parent = null;
		this.children = [];

		this.owner = [];
		this.holder = [];
	}

	saveJSON() {
		var copy = removeReferences(this);

		return JSON.stringify(copy);
	}
}

class Place extends Thing {
	constructor(type) {
		super(type);
	}

	addChild(child) {
		this.children.push(child);
		child.parent = this;
	}

	getID() {
		return this.uuid;
	}

	getChildren() {
		return this.children;
	}

	getParent() {
		return this.parent;
	}
}

class Being extends Thing {
	constructor(type) {
		super(type);

		this.property = [];
	}
}

class SuperBubble extends Place {
	constructor() {
		super('super bubble');
	}

	static random() {
		var root = new SuperBubble();
		for (var i = 10 - 1; i >= 0; i--) {
			root.addChild( SolarSystem.random() );
		}
		return root;
	}
}

class SolarSystem extends Place {
	constructor() {
		super('solar system');
	}

	static random() {
		var root = new SolarSystem();
		for (var i = 10 - 1; i >= 0; i--) {
			root.addChild( Planet.random() );
		}
		return root;
	}
}

class Planet extends Place {
	constructor() {
		super('planet');
	}

	static random() {
		var root = new Planet();
		for (var i = 10 - 1; i >= 0; i--) {
			root.addChild( City.random() );
		}
		return root;
	}
}

String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

class City extends Place {
	constructor() {
		super('city');
	}

	act() {
		if (Math.random() > 0.3) {
			this.addProperty( new Ship() );
		}
	}

	static random() {
		var root = new City();
		root.name = City.randomName();

		//TEMP
		//Establish government
		//var found

		return root;
	}

	static randomName() {
		var start = ['la', 'na', 'to', 'gre', 'sun', 'ple', 'pla', 'ro', 'fla', 'flo', 'kna', 'va', 'co', 'vic', 'tri', 'bu'];
		var end = ['ven', 'gne', 'dar', 'ler', 'mo', 'mor', 'dum', 'ver', 'ria', 'te', 'te', 'nto', 't', 'ville', 'town'];


		var part1 = start[Math.floor(Math.random() * start.length)];
		var part2 = start[Math.floor(Math.random() * start.length)];
		var part3 = end[Math.floor(Math.random() * end.length)];
		return (part1 + part2 + part3).capitalizeFirstLetter();

		var myArray = ['Cologne', 'Greven', 'Holer', 'Tribut', 'Lafidar', 'Nanaimo'];
		var rand = myArray[Math.floor(Math.random() * myArray.length)];
		return rand;
	}
}

class Federation {
	constructor() {
		console.log('The federation has been founded!');
	}
}

class Ship {
	constructor() {
		console.log('A ship has been created!');
	}
}


var sys = Planet.random();

console.log( util.inspect(sys, null, 10) );

var fs = require('fs');
fs.writeFileSync('./data.json', util.inspect({'root': sys}, null, 10) , 'utf-8');

fs.writeFileSync('./data.json', sys.saveJSON(), 'utf-8');
