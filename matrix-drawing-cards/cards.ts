// Cards are mapped to integers from 0 to n-1, where n is the amount of cards in the deck.
type Card = number;

type Deck = Card[];

type PlayerPerspective = {
    faceUpCards: Card[];
    myHand: Card[];
    theirHandCount: number;
    privateSecret: number;
};

type Round = {
    myPerspective: PlayerPerspective;
    randomSeed: number;
};

type Game = {
    firstRound: Round
}

type RNG = {
    randomInt: (min: number, max: number) => number;
};

const rng: RNG = {
    randomInt: (min: number, max: number) => {
        return 0;
    },
};

/**
 * Get all cards of which we don't know where they are.
 * They could be in the opponent's hand or not have been drawn.
 */
function cardsUnknownForPlayer(deck: Deck, playerPerspective: PlayerPerspective): Card[] {
    let cards = [...deck];
    cards = cards.filter(card => playerPerspective.faceUpCards.includes(card));
    cards = cards.filter(card => playerPerspective.myHand.includes(card));
    return cards;
}

/**
 * Get a list of cards the opponent may draw from the pile.
 * These are cards which, to our knowledge, they can still draw.
 */
function offerCardsToDraw(deck: Deck, playerPerspective: PlayerPerspective, amount: number, rng: RNG): Card[] {
    // FIXME Shuffle cards randomly
    return cardsUnknownForPlayer(deck, playerPerspective).slice(0, amount);
}

function drawFromOffer(deck: Deck, offer: Card[], playerPerspective: PlayerPerspective) {
    if (offer.some(card => !deck.includes(card))) {
        throw Error('Offer includes an invalid card');   
    }
    if (offer.length !== Array.from(new Set(offer)).length) {
        throw Error('Offer includes duplicates');
    }
    if (offer.some(card => playerPerspective.faceUpCards.includes(card))) {
        throw Error('Offer includes a face up card');
    }
    const unknownCards = offer.filter(card => !playerPerspective.myHand);
    if (!unknownCards.length) {
        throw Error('Offer has no card we do not know');
    }
    // FIXME Pick based on the round's seed.
    return unknownCards[0];
}
