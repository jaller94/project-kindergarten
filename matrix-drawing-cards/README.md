# Playing cards on Matrix

Drawing a deck of cards online with minimal trust towards the other player.

## Problem

A stranger and I want to draw randomised cards from a deck without known which cards the other person has. All communication is done online just between the two of us. There's no physical card deck.

## Goals

During the game we want to ensure no person can swing a randomized outcome in their favour or gain certain knowledge of any face down playing card.
At the end of the game we want proof that the other person didn't cheat any outcome.

## Notes

Authors: Timo and me.

```
Round: 
    {
      timestamp -> THash,

    publicCards: Cards[],

    handCardsCountA: int,

    handCardsCountB: int,

    (publicCards, handCardsCountA, handCardsCountB) -> cardsDrawable: int

    userA {

          privatHashA

    randomA=Rand(THash+privateHashA),

    handCardsA: Card[],

    }

    userB {

         privatHashB

    randomB=Rand(THash+privateHashB),

    handCardsB : Card[],

    }

    }

It is only possible to draw one card. PLAYER B DRAWS CARD

possibleProposals: Cards[] = cardsUnknownForPlayer().excluding(handCardsA)

proposedCards: Cards[] = randomA.shuffle(possibleProposals.)[0...proposeAmount]

selectedCard: Card = randomB.shuffle(proposedCards.excluding(handCardsB))[0]

// hier muss noch nen bisschen Mathe gemacht werden um zu sagen was wirklich am wenigsten info preisgibt
minProposeAmount: handCardsCountB + 1
maxProposeAmount: int = cardsDrawable - handCardsB.size
proposedAmount = round((minProposeAmount+maxProposeAmount)/2)
```
