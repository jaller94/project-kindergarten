console.log('hi');

function braileChar(char) {
  const template = document.querySelector('#braille-template');
  const clone = template.content.cloneNode(true);
  console.log(char);
  console.log('⠁');
  if (char === 'A') fillBrailleChar(clone, true, false, false, false, false, false);
  if (char === 'B') fillBrailleChar(clone, true, true, false, false, false, false);
  if (char === 'C') fillBrailleChar(clone, true, false, false, true, false, false);
  return clone;
}

function fillBrailleChar(brailleNode, dot1, dot2, dot3, dot4, dot5, dot6) {
  const dots = [
    brailleNode.querySelector('.dot1'),
    brailleNode.querySelector('.dot2'),
    brailleNode.querySelector('.dot3'),
    brailleNode.querySelector('.dot4'),
    brailleNode.querySelector('.dot5'),
    brailleNode.querySelector('.dot6'),
  ];
  if (dot1) dots[0].classList.add('filled');
  if (dot2) dots[1].classList.add('filled');
  if (dot3) dots[2].classList.add('filled');
  if (dot4) dots[3].classList.add('filled');
  if (dot5) dots[4].classList.add('filled');
  if (dot6) dots[5].classList.add('filled');
  return brailleNode;
}

function writeBraile(text) {
  const div = document.createElement('div');
  for (const char of text) {
    div.appendChild(braileChar(char));
  }
  return div;
}

document.body.appendChild(writeBraile('ABCDEFGHIJKLMNOPQRSTUVWXYZ'));
