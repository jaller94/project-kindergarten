import React, { Component } from 'react';
import './Navigation.css';

class Navigation extends Component {

  handleArrowClick = (event) => {
    if (event.target.textContent === 'Up') {
      console.log('up');
    }
  }

  render() {
    return (
      <div className="Navigation">
        {/* eslint-disable jsx-a11y/no-access-key */}
        <button onClick={this.props.handleInventoryClick} disabled accessKey="E">Inv.</button>
    		<button onClick={this.handleArrowClick} disabled accessKey="W">Up</button>
    		<button onClick={this.props.handleMeClick} disabled accessKey="I">Me</button>
    		<button onClick={this.handleArrowClick} disabled accessKey="A">Left</button>
    		<button onClick={this.handleArrowClick} disabled accessKey="S">Down</button>
    		<button onClick={this.handleArrowClick} disabled accessKey="D">Right</button>
        {/* eslint-enable jsx-a11y/no-access-key */}
      </div>
    );
  }
}

export default Navigation;
