import React, { Component } from 'react';

class StoryText extends Component {
  render() {
    return (
      <div className="StoryText">
        <p>
          {this.props.text}
        </p>
      </div>
    );
  }
}

export default StoryText;
