import React, { Component } from 'react';

class ActionMenu extends Component {
  render() {
    return (
      <div className="ActionMenu">
        <button>Action 1</button>
        <button>Action 2</button>
        <button>Action 3</button>
      </div>
    );
  }
}

export default ActionMenu;
