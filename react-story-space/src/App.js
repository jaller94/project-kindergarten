import React, { Component } from 'react';
import Navigation from './Navigation';
import Map from './Map';
import StoryText from './StoryText';
import ActionMenu from './ActionMenu';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isNavigationEnabled: false,
      isMapEnables: false,
    }
  }

  handleArrowClick = (event) => {

  };

  handleInventoryClick = (event) => {

  };

  handleMeClick = (event) => {

  };

  render() {
    return (
      <div className="App">
        <Navigation
          handleArrowClick={this.handleMeClick}
          handleInventoryClick={this.handleMeClick}
          handleMeClick={this.handleMeClick}
        />
        <Map />
        <StoryText text="This is the text to show." />
        <ActionMenu />
      </div>
    );
  }
}

export default App;
